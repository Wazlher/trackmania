﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class PlayGame : MonoBehaviour {

    [MenuItem("Play/Play")]
    public static void PlayFromPrelaunchScene()
    {
        EditorApplication.SaveCurrentSceneIfUserWantsTo();
        EditorApplication.OpenScene("Assets/Scene/SplashScreen.unity");
        EditorApplication.isPlaying = true;
    }
}
