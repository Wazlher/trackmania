﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Editeur;
//using UnityEditor;
using System.IO;
using System.Xml.Serialization;
//using UnityEditor;

public class GUIEditor : MonoBehaviour
{
    #region variables
    GameSystem gameSystem;
    Texture2D[] toolsTex;
    GUIStyle toolsStyle;
    public List<GameObject> assetsPrefab;
    public List<Texture2D> assetsTexture;
    private List<GameObject> track;

    public GameObject selectionCube;
    private GameObject currentElement;
    private GameObject destroyOver;
    private Vector3 prevMousePosition;
    private bool canSelect;

    private bool display = false;
    private int index = 0;
    private int end = 0;

    private bool destroyMode;

    private string filename;
    private string oldFilename;
    private bool needTrackName;
    private bool needToSave;

    public GameObject prefabSelection;
    List<SelectionCube> listSelection;

    private bool startTrack;
    private bool startEndTrack;
    private bool endTrack;
    private int nbCheckpoint;
    private string messageError;

    private bool first = true;
    #endregion

    #region Initialization
    void Start()
    {
        // 0 = Save ; 1 = Play ; 2 = Destroy ; 3 = Return;
        toolsTex = new Texture2D[12];
        toolsTex[0] = Resources.Load("Editeur/Save", typeof(Texture2D)) as Texture2D;
        toolsTex[1] = Resources.Load("Editeur/Play", typeof(Texture2D)) as Texture2D;
        toolsTex[2] = Resources.Load("Editeur/Destroy", typeof(Texture2D)) as Texture2D;
        toolsTex[3] = Resources.Load("Editeur/Return", typeof(Texture2D)) as Texture2D;
        toolsTex[4] = Resources.Load("Editeur/1", typeof(Texture2D)) as Texture2D;
        toolsTex[5] = Resources.Load("Editeur/15", typeof(Texture2D)) as Texture2D;
        toolsTex[6] = Resources.Load("Editeur/19", typeof(Texture2D)) as Texture2D;
        toolsTex[7] = Resources.Load("Editeur/back", typeof(Texture2D)) as Texture2D;
        toolsTex[8] = Resources.Load("Editeur/drapeauVert", typeof(Texture2D)) as Texture2D;
        toolsTex[9] = Resources.Load("Editeur/drapeauRouge", typeof(Texture2D)) as Texture2D;
        toolsTex[10] = Resources.Load("Editeur/drapeauBleu", typeof(Texture2D)) as Texture2D;
        toolsTex[11] = Resources.Load("Editeur/drapeauNoir", typeof(Texture2D)) as Texture2D;
        gameSystem = GameObject.FindGameObjectWithTag("GameSystem").GetComponent<GameSystem>() as GameSystem;

        track = new List<GameObject>();
        currentElement = null;
        canSelect = true;

        destroyMode = false;

        PersistantGame pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
        filename = pg.levelName;
        pg.levelName = string.Empty;
        pg.editorMode = false;
        needTrackName = false;
        needToSave = false;

        listSelection = new List<SelectionCube>();

        LoadTrack();
        messageError = string.Empty;
    }
    #endregion

    void OnGUI()
    {
        /*    if (first)
            {
                foreach (GameObject game in assetsPrefab)
                {
                    Texture2D tex = AssetPreview.GetAssetPreview(game);
                    Debug.Log("Game name : " +  game.name + " Tex name = : " + tex);
                    byte[] bytes = tex.EncodeToPNG();
                    File.WriteAllBytes(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "AssetsTexture/" + game.name + ".png"), bytes);
                }
                first = false;
            }*/
        int h = Screen.height;
        int w = Screen.width;

        if (!needTrackName && !needToSave)
            CheckMouseInput();

        CreateOneBox(toolsTex[3], new Rect(0, h - 87, 75, 40), ReturnMainMenu);
        CreateOneBox(toolsTex[0], new Rect(0, h - 50, 50, 50), SaveTrack);
        CreateOneBox(toolsTex[1], new Rect(50, h - 50, 50, 50), PlayTrack);
        CreateOneBox(toolsTex[2], new Rect(100, h - 50, 50, 50), DestroyMode);
        if (display == false)
        {
            CreateOneBox(toolsTex[4], new Rect(160, h - 70, 65, 65), DisplayFirst);
            CreateOneBox(toolsTex[5], new Rect(230, h - 70, 65, 65), DisplaySecond);
            CreateOneBox(toolsTex[6], new Rect(300, h - 70, 65, 65), DisplayThird);
            CreateOneBox(toolsTex[8], new Rect(400, h - 70, 65, 65), DisplayStart);
            CreateOneBox(toolsTex[9], new Rect(500, h - 70, 65, 65), DisplayFinish);
            CreateOneBox(toolsTex[10], new Rect(600, h - 70, 65, 65), DisplayStartAndFinish);
            CreateOneBox(toolsTex[11], new Rect(700, h - 70, 65, 65), DisplayCheckpoint);
        }
        CreateAssetsBox(new Rect(150, h - 72, w - 150, 70));

        if (needTrackName)
            SaveTrackGUI(w, h);

        if (needToSave)
        {
            if (!((startEndTrack && nbCheckpoint >= 1) || (startTrack && endTrack)))
            {
                messageError = "You need to have one start and one end or one start-end loop with one checkpoint to play";
                needToSave = false;
            }
            else
                PlayTrackGUI(w, h);
        }

        if (messageError != string.Empty)
            MessageBox(w, h);

        TileMapMouse tm = GameObject.Find("TileMap").GetComponent<TileMapMouse>();
        foreach (SelectionCube cube in listSelection)
        {
            Vector3 pos = cube.obj.transform.position;
            pos.x = gameSystem.x + (cube.posX * 5.0f);
            pos.z = gameSystem.z + (cube.posY * 5.0f);
            cube.obj.transform.position = pos;
        }
    }

    #region DisplayGUI
    private void CreateOneBox(Texture2D tex, Rect rect, Action act)
    {
        GUILayout.BeginArea(rect);
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(tex) && !needTrackName && !needToSave)
        {
            selectionCube.renderer.material.color = new Color32(0, 255, 95, 131);
            destroyMode = false;
            act();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void CreateAssetsBox(Rect rect)
    {
        if (display == false)
            return;
        GUILayout.BeginArea(rect);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        for (int i = index; i < end; ++i)
        {
            if (GUILayout.Button(assetsTexture[i], GUILayout.Width(70), GUILayout.Height(70)) &&
                !needToSave && !needTrackName)
            {
                selectionCube.renderer.material.color = new Color32(0, 255, 95, 131);
                destroyMode = false;
                if (!ResetStartEndValues(true, assetsPrefab[i].tag))
                    messageError = "Track have to be only one start and one end, or one start-end loop";
                else
                {
                    GameObject go = Instantiate(assetsPrefab[i]) as GameObject;
                    go.transform.position = new Vector3(gameSystem.x, 0.0f, gameSystem.z);
                    if (assetsPrefab[i].tag.Equals("Start"))
                        AddTagRecursively(go.transform, "Start");
                    else if (assetsPrefab[i].tag.Equals("Finish"))
                        AddTagRecursively(go.transform, "Finish");
                    else if (assetsPrefab[i].tag.Equals("StartAndFinish"))
                        AddTagRecursively(go.transform, "StartAndFinish");
                    else if (assetsPrefab[i].tag.Equals("Checkpoint"))
                    {
                        nbCheckpoint++;
                        AddTagRecursively(go.transform, "Checkpoint");
                    }
                    else
                        AddTagRecursively(go.transform, "ToSave");
                    currentElement = go;
                    track.Add(go);
                    Vector2 nbSquare = GetNumberofSquare(go.name);
                    InstantiateSelectionCubes(nbSquare);
                }
            }
        }
        if (GUILayout.Button(toolsTex[7], GUILayout.Width(70), GUILayout.Height(70)) &&
            !needToSave && !needTrackName)
        {
            selectionCube.renderer.material.color = new Color32(0, 255, 95, 131);
            destroyMode = false;
            display = false;
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void SaveTrackGUI(int w, int h)
    {
        GUILayout.BeginArea(new Rect(w / 2 - 150, h / 2 - 40, 300, 80));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        filename = GUILayout.TextField(filename);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Cancel"))
        {
            filename = oldFilename;
            needTrackName = false;
        }
        if (GUILayout.Button("Save"))
        {
            if (filename != string.Empty)
            {
                needTrackName = false;
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/" + filename + ".xml");
                GameObject[] gameobjectsArray = GameObject.FindGameObjectsWithTag("ToSave") as GameObject[];
                foreach (GameObject go in gameobjectsArray)
                    Debug.LogWarning(go.name);
                SaveTrack st = new SaveTrack(gameobjectsArray, path);
                st.Save();
                StartCoroutine(SaveImage());
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    private void PlayTrackGUI(int w, int h)
    {
        GUILayout.BeginArea(new Rect(w / 2 - 150, h / 2 - 40, 300, 80));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.Label("You have to save your track before play with it");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Cancel"))
            needToSave = false;
        if (GUILayout.Button("Save"))
        {
            needToSave = false;
            needTrackName = true;
        }
        GUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    private void MessageBox(int w, int h)
    {
        GUILayout.BeginArea(new Rect(w / 2 - 150, h / 2 - 40, 500, 80));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.Label(messageError);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Ok"))
            messageError = string.Empty;
        GUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
    #endregion

    #region Mouse Events
    private void CheckMouseInput()
    {
        Event e = Event.current;
        if (destroyMode)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "ToSave" || hit.transform.tag == "Start" ||
                    hit.transform.tag == "Finish" || hit.transform.tag == "StartAndFinish" ||
                    hit.transform.tag == "Checkpoint")
                {
                    if (destroyOver)
                        ChangeMaterialsColor(Color.white, destroyOver.renderer.materials);
                    destroyOver = SearchForFirstChild(hit.transform.gameObject);
                    ChangeMaterialsColor(Color.red, destroyOver.renderer.materials);
                }
                else if (destroyOver)
                {
                    ChangeMaterialsColor(Color.white, destroyOver.renderer.materials);
                    destroyOver = null;
                }
            }
            else if (destroyOver)
            {
                ChangeMaterialsColor(Color.white, destroyOver.renderer.materials);
                destroyOver = null;
            }
        }
        if (e.button == 0 && e.isMouse && e.type == EventType.MouseDown && canSelect)
        {
            canSelect = false;
            if (currentElement != null)
            {
                currentElement = null;
                DestroySelectionCube();
            }
            else if (destroyMode && destroyOver)
            {
                track.Remove(destroyOver);
                if (destroyOver.transform.tag == "Checkpoint")
                    nbCheckpoint--;
                ResetStartEndValues(false, destroyOver.tag);
                Destroy(destroyOver.transform.parent.gameObject);
                destroyOver = null;
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.tag == "ToSave" || hit.transform.tag == "Start" ||
                        hit.transform.tag == "Finish" || hit.transform.tag == "StartAndFinish" ||
                        hit.transform.tag == "Checkpoint")
                        currentElement = hit.transform.gameObject;
                }
            }
        }
        else if (e.button == 1 && currentElement && e.isMouse && e.type == EventType.MouseDown && canSelect)
        {
            Vector3 rot = currentElement.transform.rotation.eulerAngles;
            rot.y += 90.0f;
            currentElement.transform.rotation = Quaternion.Euler(rot);
            RotateSquares();
        }
        else if ((e.button == 0 || e.button == 1) && e.isMouse && e.type == EventType.MouseUp && !canSelect)
        {
            canSelect = true;
        }
        if (currentElement)
        {
            Vector3 newPosition = new Vector3(gameSystem.x, 0, gameSystem.z);
            Vector3 realPosition = GetSquarePosition(newPosition);
            currentElement.transform.position = realPosition;
            prevMousePosition = Input.mousePosition;
        }
    }

    void RotateSquares()
    {
        if (currentElement.transform.rotation.eulerAngles.y >= 85.0f &&
            currentElement.transform.rotation.eulerAngles.y <= 95.0f)
        {
            Vector2 nbSquare = GetNumberofSquare(currentElement.name);
            float tmp = nbSquare.x;
            nbSquare.x = nbSquare.y;
            nbSquare.y = tmp;
            InstantiateSelectionCubes(nbSquare);
        }
        else
        {
            Vector2 nbSquare = GetNumberofSquare(currentElement.name);
            InstantiateSelectionCubes(nbSquare);
        }
    }

    private Vector3 GetMousePositionInWorld(float y)
    {
        Vector3 v3 = Input.mousePosition;
        v3.z = 100.0f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        v3.y = y;
        return v3;
    }

    private Vector3 GetSquarePosition(Vector3 current)
    {
        if (listSelection.Count > 0)
        {
            current.x += listSelection[0].offsetX;
            current.z += listSelection[0].offsetY;
        }
        return current;
    }
    #endregion

    #region ButtonDelegate
    private void ReturnMainMenu()
    {
        Fade.loadLevel("MainMenu", 2.0f, 1.0f, Color.black);
    }

    private void SaveTrack()
    {
        if ((startTrack && endTrack) || (startEndTrack && nbCheckpoint >= 1))
        {
            oldFilename = filename;
            needTrackName = true;
        }
        else
            messageError = "You need to have one start and one finish or one start-finish loop and one checkpoint";
    }

    private void DestroyMode()
    {
        destroyMode = true;
        //    selectionCube.renderer.material.color = new Color32(102, 2, 165, 131);
    }

    private void PlayTrack()
    {
        if (filename == string.Empty)
        {
            oldFilename = string.Empty;
            needToSave = true;
        }
        else if ((startTrack && endTrack) || (startEndTrack && nbCheckpoint >= 1))
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/" + filename + ".xml");
            #region ArrayManipulation
            List<GameObject> gameobjectList = new List<GameObject>();
            LoopGameObjectArrayByTag(gameobjectList, "ToSave");
            LoopGameObjectArrayByTag(gameobjectList, "Start");
            LoopGameObjectArrayByTag(gameobjectList, "Finish");
            LoopGameObjectArrayByTag(gameobjectList, "StartAndFinish");
            LoopGameObjectArrayByTag(gameobjectList, "Checkpoint");
            GameObject[] gameobjectsArrays = gameobjectList.ToArray();
            #endregion ArrayManipulation
            SaveTrack st = new SaveTrack(gameobjectsArrays, path);
            st.Save();
            StartCoroutine(SaveImage());

            PersistantGame pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
            pg.levelName = filename;
            pg.editorMode = true;
            Fade.loadLevel("Terrain_prefab", 0.5f, 0.5f, Color.black);
        }
        else
            messageError = "You need to have one start and one finish or one start-finish loop and one checkpoint";
    }

    void LoopGameObjectArrayByTag(List<GameObject> list, string tag)
    {
        GameObject[] gameobjectsArray = GameObject.FindGameObjectsWithTag(tag) as GameObject[];
        foreach (GameObject go in gameobjectsArray)
            list.Add(go);
    }

    private void DisplayFirst()
    {
        display = true;
        index = 0;
        end = 4;
    }

    private void DisplaySecond()
    {
        display = true;
        index = 4;
        end = 9;
    }

    private void DisplayThird()
    {
        display = true;
        index = 9;
        end = 13;
    }

    private void DisplayStart()
    {
        display = true;
        index = 13;
        end = 16;
    }

    private void DisplayFinish()
    {
        display = true;
        index = 16;
        end = 19;
    }

    private void DisplayStartAndFinish()
    {
        display = true;
        index = 19;
        end = 22;
    }

    private void DisplayCheckpoint()
    {
        display = true;
        index = 22;
        end = 25;
    }
    #endregion

    private void LoadTrack()
    {
        if (filename != string.Empty)
        {
            string pathname = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/" + filename + ".xml");
            XmlSerializer xs = new XmlSerializer(typeof(List<EditorAsset>));
            using (StreamReader sr = new StreamReader(pathname))
            {
                List<EditorAsset> eas = xs.Deserialize(sr) as List<EditorAsset>;
                foreach (EditorAsset ea in eas)
                {
                    GameObject go = Resources.Load("Editeur/" + ea.name, typeof(GameObject)) as GameObject;
                    Instantiate(go, ea.position, ea.rotation);
                    track.Add(go);
                    go.tag = ea.tag;
                    AddTagRecursively(go.transform, ea.tag);
                }
            }
        }
    }

    private IEnumerator SaveImage()
    {
        yield return new WaitForEndOfFrame();
        Texture2D captured = new Texture2D(Screen.width, Screen.height);
        camera.Render();
        RenderTexture.active = camera.targetTexture;
        captured.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        captured.Apply();
        RenderTexture.active = null;
        byte[] bytes = captured.EncodeToPNG();
        File.WriteAllBytes(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Images/" + filename + ".png"), bytes);
    }

    private Vector2 GetNumberofSquare(string name)
    {
        Vector2 ret = new Vector2(0, 0);

        if (name.Equals("Checkpoint(Clone)"))
        {
            ret.x = 3;
            ret.y = 1;
        }
        if (name.Equals("Long turn road(Clone)"))
        {
            ret.x = 2;
            ret.y = 2;
        }
        if (name.Equals("Ramp road(Clone)"))
        {
            ret.x = 1;
            ret.y = 2;
        }
        if (name.Equals("Small turn road(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Straight road(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Looping(Clone)"))
        {
            ret.x = 2;
            ret.y = 4;
        }
        if (name.Equals("Road_Pont(Clone)"))
        {
            ret.x = 1;
            ret.y = 4;
        }
        if (name.Equals("Road_Ramp(Clone)"))
        {
            ret.x = 1;
            ret.y = 2;
        }
        if (name.Equals("Road_Straight(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Turn_Long(Clone)"))
        {
            ret.x = 2;
            ret.y = 2;
        }
        if (name.Equals("Road_Turn_Short(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Curve_Large(Clone)"))
        {
            ret.x = 2;
            ret.y = 2;
        }
        if (name.Equals("Road_Curve_Short(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Ramp_Curve(Clone)"))
        {
            ret.x = 2;
            ret.y = 2;
        }
        if (name.Equals("Road_Ramp_Straight(Clone)"))
        {
            ret.x = 1;
            ret.y = 2;
        }
        if (name.Equals("Road_Straight_01(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Straight_02(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Start_1(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Start_15(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Start_19(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_StartAndFinish_1(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_StartAndFinish_15(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_StartAndFinish_19(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Finish_1(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Finish_15(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Finish_19(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Checkpoint_1(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Checkpoint_15(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        if (name.Equals("Road_Checkpoint_19(Clone)"))
        {
            ret.x = 1;
            ret.y = 1;
        }
        return ret;
    }

    private void DestroySelectionCube()
    {
        foreach (SelectionCube cube in listSelection)
            Destroy(cube.obj);
        listSelection = new List<SelectionCube>();
    }

    private void InstantiateSelectionCubes(Vector2 vec)
    {
        int x = (int)vec.x;
        int y = (int)vec.y;
        int midX = (x / 2);
        int midY = (y / 2);

        if (x % 2 == 0)
            midX--;
        if (y % 2 == 0)
            midY--;


        DestroySelectionCube();
        for (int i = -(x / 2); i <= midX; ++i)
        {
            for (int j = -(y / 2); j <= midY; ++j)
            {
                GameObject tmp = Instantiate(prefabSelection) as GameObject;
                SelectionCube cube = new SelectionCube(i, j, tmp, 0f, 0f);
                if (x % 2 == 0)
                    cube.offsetX = -2.5f;
                if (y % 2 == 0)
                    cube.offsetY = -2.5f;
                listSelection.Add(cube);
            }
        }
    }

    void ChangeMaterialsColor(Color col, Material[] mats)
    {
        foreach (Material mat in mats)
            mat.color = col;
    }

    GameObject SearchForFirstChild(GameObject child)
    {
        if (child.transform.parent == null)
            return child.transform.GetChild(0).gameObject;
        else
            return SearchForFirstChild(child.transform.parent.gameObject);
    }

    void AddTagRecursively(Transform trans, string tag)
    {
        trans.gameObject.tag = tag;
        if (trans.GetChildCount() > 0)
            foreach (Transform t in trans)
                AddTagRecursively(t, tag);
    }

    bool ResetStartEndValues(bool add, string tag)
    {
        if (add)
        {
            if (tag.Equals("Start"))
            {
                if (startTrack == true || startEndTrack == true)
                    return false;
                startTrack = true;
            }
            if (tag.Equals("Finish"))
            {
                if (endTrack == true || startEndTrack == true)
                    return false;
                endTrack = true;
            }
            if (tag.Equals("StartAndFinish"))
            {
                if (startTrack == true || endTrack == true || startEndTrack == true)
                    return false;
                startEndTrack = true;
            }
        }
        else
        {
            if (tag.Equals("Start"))
                startTrack = false;
            if (tag.Equals("Finish"))
                endTrack = false;
            if (tag.Equals("StartAndFinish"))
                startEndTrack = false;
        }
        return true;
    }
}
