using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TileMap))]
public class TileMapMouse : MonoBehaviour {
	
	TileMap _tileMap;
	
	public Vector3 currentTileCoord;
	
	public Transform selectionCube;
	
	void Start() {
		_tileMap = GetComponent<TileMap>();
	}

	// Update is called once per frame
	void Update () {
		Ray ray = Camera.mainCamera.ScreenPointToRay( Input.mousePosition );
		RaycastHit hitInfo;
		
		if( collider.Raycast( ray, out hitInfo, Mathf.Infinity ) ) {
			int x = Mathf.FloorToInt( hitInfo.point.x / _tileMap.tileSize);
			int z = Mathf.FloorToInt( hitInfo.point.z / _tileMap.tileSize);
			//Debug.Log ("Tile: " + x + ", " + z);
			
			currentTileCoord.x = x;
			currentTileCoord.z = z;

            GameSystem Save;
            Save = GameObject.FindGameObjectWithTag("GameSystem").GetComponent<GameSystem>() as GameSystem;

            Save.x = (currentTileCoord.x * 5.0f) + (2.5f);
            Save.z = (currentTileCoord.z * 5.0f) + (2.5f);
		}
		else {
			// Hide selection cube?
		}
		
		if(Input.GetMouseButtonDown(0))
		{
		//	Debug.Log ("Click!");
		/*	print("pos X MAP: " + currentTileCoord.x);
			print("pos Z MAP: " + currentTileCoord.z);
			print("pos X WORLD: " + ((currentTileCoord.x * 5.0f) + (2.5f)));
			print("pos Z WORLD: " + ((currentTileCoord.z * 5.0f) + (2.5f)));*/
		}
	}
}
