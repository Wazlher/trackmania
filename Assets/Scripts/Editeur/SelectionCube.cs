﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Editeur
{
    public class SelectionCube
    {
        public float posX { get; private set; }
        public float posY { get; private set; }
        public float offsetX { get; set; }
        public float offsetY { get; set; }

        public GameObject obj;

        public SelectionCube(int x, int y, GameObject g, float offsetX, float offsetY)
        {
            posX = x;
            posY = y;
            offsetX = offsetX;
            offsetY = offsetY;
            obj = g;
        }
    }
}
