﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System;
using System.Xml.Serialization;
using Assets.Scripts.Editeur;

public class SaveTrack
{
    GameObject[] gameobjectsArray;
    string filename;

    public SaveTrack(GameObject[] goArray, string path)
    {
        gameobjectsArray = goArray;
        filename = path;
    }

    public void Save()
    {
        List<EditorAsset> eaList = new List<EditorAsset>();

        foreach (GameObject go in gameobjectsArray)
        {
            if (go.transform.parent == null)
                eaList.Add(new EditorAsset(go));
        }

        XmlSerializer xs = new XmlSerializer(typeof(List<EditorAsset>));
        using (StreamWriter wr = new StreamWriter(filename))
        {
           xs.Serialize(wr, eaList);
        }
    }
}
