﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.Editeur
{
    public class EditorAsset
    {
        public string name { get; set; }
        public Vector3 position { get; set; }
        public Quaternion rotation { get; set; }
        public string tag { get; set; }

        public EditorAsset()
        {
        }

        public EditorAsset(GameObject go)
        {
            name = go.name;
            if (name.Contains("(Clone)"))
                name = name.Remove(name.Length - 7); // Remove "(Clone)"
            position = go.transform.position;
            rotation = go.transform.rotation;
            tag = go.transform.tag;
        }
    }
}
