﻿using UnityEngine;
using System.Collections;

public class CameraEditor : MonoBehaviour
{
	public GameObject rotatePoint;
	public GameObject camera;
	public Vector2 lastPosition;

	void Start ()
	{
	}
	
	void Update ()
	{
		CheckHorizontalInput();
		CheckVerticalInput();
		VerifValue();
		CheckWheelInput();
	}

	void OnGUI()
	{
		CheckMouseInput();
		CheckClic();
	}

	private void CheckMouseInput()
	{
		if (!Input.GetButton("Fire3"))
		{
			CheckOneSideMouse(0.0f, Screen.height / 20, 0.0f, Screen.width, transform.forward);
			CheckOneSideMouse(19 * (Screen.height / 20), Screen.height, 0.0f, Screen.width, -transform.forward);
			CheckOneSideMouse(0.0f, Screen.height, 0.0f, (Screen.width / 20) + 10, -transform.right);
			CheckOneSideMouse(0.0f, Screen.height, 19 * (Screen.width / 20), Screen.width, transform.right);
		}
	}

	private void CheckOneSideMouse(float upY, float botY, float upX, float botX, Vector3 param)
	{
		Vector2 mousePos = Event.current.mousePosition;
		param = param * 20.0f * Time.deltaTime;
		if (mousePos.x < 150 && mousePos.y > 200)
			return;
		if (mousePos.y >= upY && mousePos.y <= botY &&
			mousePos.x >= upX && mousePos.x <= botX)
		{
			transform.Translate(param.x, 0.0f, param.z, Space.World);
		}
	}

	private void CheckClic()
	{
		Vector2 val = Event.current.mousePosition;
		if (Input.GetButton("Fire3"))
		{
			transform.Rotate(Vector3.up, 20 * Time.deltaTime * (lastPosition.x - val.x), Space.World);
			if (20 * Time.deltaTime * (lastPosition.y - val.y) + rotatePoint.transform.localRotation.eulerAngles.x < 290 && rotatePoint.transform.localRotation.eulerAngles.x > 270)
			{
				rotatePoint.transform.Rotate(rotatePoint.transform.right, 290 - rotatePoint.transform.localRotation.eulerAngles.x, Space.World);
			}
			else if (((20 * Time.deltaTime * (lastPosition.y - val.y) + rotatePoint.transform.localRotation.eulerAngles.x) > 360) || (((20 * Time.deltaTime * (lastPosition.y - val.y) + rotatePoint.transform.localRotation.eulerAngles.x) < 270) && ((20 * Time.deltaTime * (lastPosition.y - val.y) + rotatePoint.transform.localRotation.eulerAngles.x) > 0)))
			{
				rotatePoint.transform.localRotation.SetFromToRotation(rotatePoint.transform.localRotation.eulerAngles, Vector3.zero);
			}
			else
				rotatePoint.transform.Rotate(rotatePoint.transform.right, 20 * Time.deltaTime * (lastPosition.y - val.y), Space.World);
		}
		lastPosition = val;
	}

	private void CheckHorizontalInput()
	{
		if (Input.GetAxis("Horizontal") != 0.0f)
		{
			float axis = Input.GetAxis("Horizontal");
			if (axis > 0.0f)
			{
				transform.Translate(transform.right * 50 * Time.deltaTime, Space.World);
			}
			else
			{
				transform.Translate(-transform.right * 50 * Time.deltaTime, Space.World);
			}
		}
	}

	private void CheckVerticalInput()
	{
		if (Input.GetAxis("Vertical") != 0.0f)
		{
			float axis = Input.GetAxis("Vertical");
			if (axis > 0.0f)
			{
				transform.Translate(transform.forward * 50 * Time.deltaTime, Space.World);
			}
			else
			{
				transform.Translate(-transform.forward * 50 * Time.deltaTime, Space.World);
			}
		}
	}

	private void VerifValue()
	{
		if (transform.position.x > 500)
			transform.Translate(500 - transform.position.x, 0, 0, Space.World);
		if (transform.position.x < 0)
			transform.Translate(-transform.position.x, 0, 0, Space.World);
		if (transform.position.z > 0)
			transform.Translate(0, 0, -transform.position.z, Space.World);
		if (transform.position.z < -500)
			transform.Translate(0, 0, -500 - transform.position.z, Space.World);
	}

	private void CheckWheelInput()
	{
		if (Input.GetAxis("Mouse ScrollWheel") != 0.0f)
		{
			float axis = Input.GetAxis("Mouse ScrollWheel");
			if (axis > 0.0f)
			{
				camera.transform.Translate(0, 0, 1000 * Time.deltaTime, Space.Self);
			}
			else
			{
				camera.transform.Translate(0, 0, -1000 * Time.deltaTime, Space.Self);
			}
		}
	}
}