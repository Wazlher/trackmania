﻿using UnityEngine;
using System.Collections;

public class TimerStartDuo : MonoBehaviour {	
	private float startTime;
	private string startPrint;

	private PlayersCar_C player_1;
	private CameraSwap cameraSwap_1;
	
	private PlayersCar_C player_2;
	private CameraSwap cameraSwap_2;
	
	private TimerDuo timer;
	private TimerStartDuo start;
	

	public void Start()
	{
		GameObject car_1 = GameObject.Find("CAR");
		GameObject car_2 = GameObject.Find("CAR2");
		
		player_1 = car_1.GetComponent<PlayersCar_C>();
		cameraSwap_1 = car_1.GetComponent<CameraSwap>();
		
		player_2 = car_2.GetComponent<PlayersCar_C>();
		cameraSwap_2 = car_2.GetComponent<CameraSwap>();
		
		timer = car_1.GetComponent<TimerDuo>();
		start = car_1.GetComponent<TimerStartDuo>();
		
		player_1.enabled = false;
		cameraSwap_1.enabled = false;
		player_2.enabled = false;
		cameraSwap_2.enabled = false;
		
		timer.enabled = false;
	}

	void Awake()
	{
		startTime = Time.time;
	}
	
	void OnGUI()
	{
		float guiTime = Time.time - startTime;
		int seconds = (int)guiTime % 60;
		
		if (seconds <= 0)
			startPrint = "5";
		else if (seconds <= 1)
			startPrint = "4";
		else if (seconds <= 2)
			startPrint = "3";
		else if (seconds <= 3)
			startPrint = "2";
		else if (seconds <= 4)
			startPrint = "1";
		else if (seconds <= 5)
			startPrint = "GO !";
		else if (seconds > 3)
			startPrint = "";
		
		GUIStyle startStyle = new GUIStyle(GUI.skin.label);
		startStyle.fontSize = 200;
		startStyle.normal.textColor = Color.red;
		startStyle.font = (Font)Resources.Load("Font/virgo");
		if (startPrint == "GO !")
			GUI.Label (new Rect ((Screen.width/2) - 210, (Screen.height/2) - 100, 500, 300), startPrint, startStyle);
		else
			GUI.Label (new Rect ((Screen.width/2) - 50, (Screen.height/2) - 100, 500, 300), startPrint, startStyle);
	}
	
	void Update()
	{
		if (startPrint == "")
		{
			player_1.enabled = true;
			cameraSwap_1.enabled = true;
			
			player_2.enabled = true;
			cameraSwap_2.enabled = true;
			
			timer.enabled = true;
			start.enabled = false;
		}
	}
}
