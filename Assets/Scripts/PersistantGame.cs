﻿using UnityEngine;
using System.Collections;

public class PersistantGame : MonoBehaviour {

    public string levelName;
    public bool editorMode;

    public int winner = 1;
    public int nbPlayer = 1;
    public string timeFirst = "02:35";
    public string timeSecond = "02:41";
    public string goldTime = "02:10";
    public string silverTime = "02:20";
    public string bronzeTime = "02:35";

	void Start() 
    {
        levelName = string.Empty;
        editorMode = false;
        timeFirst = "02:35";
        timeSecond = "02:41";
        goldTime = "02:10";
        silverTime = "02:20";
        bronzeTime = "02:35";
	}

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
}
