﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.IO;
using Assets.Scripts.Editeur;
using System.Collections.Generic;

public class LoadTrack : MonoBehaviour {

    PersistantGame pg;
    string filename;

	void Start () {
        pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
        filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/" + pg.levelName + ".xml");
        XmlSerializer xs = new XmlSerializer(typeof(List<EditorAsset>));
        using (StreamReader sr = new StreamReader(filename))
        {
            List<EditorAsset> eas = xs.Deserialize(sr) as List<EditorAsset>;
            int layer = LayerMask.NameToLayer("Track");
            foreach (EditorAsset ea in eas)
            {
                Debug.Log("Editeur/" + ea.name);
                GameObject go = Resources.Load("Editeur/" + ea.name, typeof(GameObject)) as GameObject;
                SetLayerRecursively(go, layer);
                Instantiate(go, ea.position, ea.rotation);
            }
        }
	}

    void SetLayerRecursively(GameObject obj, int layer)
    {
        if (null == obj)
            return;
        obj.layer = layer;
        foreach (Transform child in obj.transform)
        {
            if (null == child)
                continue;
            SetLayerRecursively(child.gameObject, layer);
        }
    }
}
