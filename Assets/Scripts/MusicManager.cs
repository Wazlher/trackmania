﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	
	public AudioClip[] sounds;
	
	string scene;
	string music;
	
	int save = 0;
	float time = 0.0f;
	
	void setMusic()
	{
		if (scene == "SplashScreen" || scene == "MainMenu")
		{
			if (music != sounds[0].name)
			{
				audio.clip = sounds[0];
				audio.Play();
				save = 0;
			}
		}
		else if (scene == "Editor" || scene == "EditorMenu")
		{
			if (music != sounds[1].name)
			{
				audio.clip = sounds[1];
				audio.Play();
				save = 1;
			}
		}
		else if (scene == "Credits")
		{
			
		}
		else if (scene == "Terrain_prefab")
		{
			if (music != sounds[2].name)
			{
				audio.clip = sounds[2];
				audio.Play();
				save = 2;
			}
			transform.position = GameObject.Find("Car").transform.position;
		}
		else
		{
			if (music != sounds[2].name)
			{
				audio.clip = sounds[2];
				audio.Play();
				save = 2;
			}
			transform.position = GameObject.Find("CAR").transform.position;
		}
	}
	
	void Update ()
	{
		scene = Application.loadedLevelName;
		music = audio.clip.name;
		setMusic();
		transform.position = Camera.main.transform.position;
	}
	
	void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
		scene = Application.loadedLevelName;
		music = audio.clip.name;
		setMusic();
    }
	
}
