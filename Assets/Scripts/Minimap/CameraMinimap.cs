﻿using UnityEngine;
using System.Collections;

public class CameraMinimap : MonoBehaviour {
	public Transform Target;
	public GameObject carMiniMap;
	
	private GameObject o;
	private Vector3 savePos;
	private Quaternion saveRot;
	
	void Start()
	{
		savePos = Target.position;
		savePos.y += 99;
		saveRot = Target.rotation;
		o = Instantiate(carMiniMap, savePos, saveRot) as GameObject;
        o.active = true;
	}
	
	void LateUpdate ()
	{
		savePos.x = Target.position.x;
		saveRot = Target.rotation;
		savePos.z = Target.position.z;
		if (o != null)
		{
			o.transform.position = savePos;
			o.transform.rotation = saveRot;
			Vector3 angles = o.transform.rotation.eulerAngles;
			angles.y += 90.0f;
            angles.z = 0.0f;
			o.transform.rotation = Quaternion.Euler(angles);
		}
	}
}
