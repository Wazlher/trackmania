﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	Color defaultColor;
	Color selectedColor;
	
	private bool clicked = false;
	TextMesh texture;
	
	public bool isClicked
	{
		get { return clicked; }
	}
	
	public void resetClick ()
	{
		this.clicked = false;
	}
	
	void applyDefaultColor ()
	{
		texture.color = defaultColor;
	}
	
	void applySelectedColor ()
	{
		texture.color = selectedColor;
	}
	
	void Awake ()
	{
		texture = GetComponentInChildren<TextMesh>();
		
		defaultColor = texture.color;
		selectedColor = Color.red;
	}
	
	void OnMouseExit ()
	{
		applyDefaultColor();
	}
	
	void OnMouseEnter ()
	{
		applySelectedColor();
	}
	
	void OnMouseUp ()
	{
		clicked = true;
	}
}
