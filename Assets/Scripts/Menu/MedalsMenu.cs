﻿using UnityEngine;
using System.Collections;

public class MedalsMenu : MonoBehaviour {

	private enum Choice
	{
		E_RIGHT,
		E_LEFT
	}
	
	int medal = 1;
	
	Choice choice;
	
	Button right = null;
	Button left = null;
	
	public GameObject Medal;
	
	public Material[] medals;
	
	public GameObject rightLabel;
	public GameObject leftLabel;

	void applyChoice ()
	{
		if (choice == Choice.E_RIGHT)
		{
			if (medal != 3)
				medal++;
			else
				medal = 1;
			Medal.renderer.material = medals[medal];
		}
		else if (choice == Choice.E_LEFT)
		{
			if (medal != 1)
				medal--;
			else
				medal = 3;
			Medal.renderer.material = medals[medal];
		}
	}
	
	void checkClicks ()
	{
		if (right.isClicked)
		{
			right.resetClick();
			choice = Choice.E_RIGHT;
			applyChoice();
		}
		else if (left.isClicked)
		{
			left.resetClick();
			choice = Choice.E_LEFT;
			applyChoice();
		}
	}
	
	void Awake ()
	{
		right = rightLabel.GetComponent<Button>();
		left = leftLabel.GetComponent<Button>();
		if (!right || !left)
			return;
		
		Medal.renderer.material = medals[0];
	}
	
	void Update ()
	{
		checkClicks();
	}
}
