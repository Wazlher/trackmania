﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {
	
	public Color fadeColor;
	float fadeOutTime = 1.0f;
	
	IEnumerator Wait ()
	{
		yield return new WaitForSeconds(60.0f);
		Fade.loadLevel("MainMenu", fadeOutTime, 1, fadeColor);
	}
	
	void Update ()
	{
		StartCoroutine(Wait());
		if (Input.GetKeyDown(KeyCode.Escape))
			Fade.loadLevel("MainMenu", fadeOutTime, 1, fadeColor);
	}
}
