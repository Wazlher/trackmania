﻿using UnityEngine;
using System.Collections;

public class PlayersMenu : MonoBehaviour {

	private enum Choice
	{
		E_RIGHT,
		E_LEFT
	}
	
	public static int player = 1;
	
	Choice choice;
	
	Button left = null;
	Button right = null;
	
	public GameObject playerLabel;
	public GameObject rightLabel;
	public GameObject leftLabel;
	
	public GameObject medalsmenuLabel;
	
	GameObject medalMenu;
	
	void changePercentLabel(GameObject label, int percent)
	{
		TextMesh text = label.GetComponent<TextMesh>();
		text.text = percent.ToString();
	}
	
	void applyChoice ()
	{
		if (choice == Choice.E_RIGHT && player!= 2)
		{
			player++;
			changePercentLabel(playerLabel, player);
			if (player > 1)
				Destroy (GameObject.Find("MedalsMenu(Clone)"));
		}
		else if (choice == Choice.E_LEFT && player != 1)
		{
			player--;
			changePercentLabel(playerLabel, player);
			if (player == 1)
				medalMenu = Instantiate(medalsmenuLabel) as GameObject;
		}
	}
	
	void checkClicks ()
	{
		if (right.isClicked)
		{
			right.resetClick();
			choice = Choice.E_RIGHT;
			applyChoice();
		}
		else if (left.isClicked)
		{
			left.resetClick();
			choice = Choice.E_LEFT;
			applyChoice();
		}
	}
	
	void Awake ()
	{		
		changePercentLabel(playerLabel, player);
		
		right = rightLabel.GetComponent<Button>();
		left = leftLabel.GetComponent<Button>();
		if (!right || !left)
			return;
	}
	
	void Update ()
	{
		checkClicks();
	}
}
