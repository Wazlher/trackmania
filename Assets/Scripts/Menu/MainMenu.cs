﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	private enum Choice
	{
		E_PLAY,
		E_EDITOR,
		E_OPTIONS,
		E_CREDITS,
		E_QUIT
	}
	
	public Color fadeColor;
	float fadeOutTime = 1.0f;
	
	Choice choice = Choice.E_PLAY;
	
	Button play = null;
	Button editor = null;
	Button options = null;
	Button credits = null;
	Button quit = null;
	
	GameObject medalMenu;
	GameObject playersMenu;
	
	public GameObject playmenuLabel;
	public GameObject optionsmenuLabel;
	public GameObject medalsmenuLabel;
	public GameObject playersmenuLabel;
	
	public GameObject playLabel;
	public GameObject editorLabel;
	public GameObject optionsLabel;
	public GameObject creditsLabel;
	public GameObject quitLabel;
	
	IEnumerator StartPlay ()
	{
		animation.Play ("Back");
		playmenuLabel.animation.Play ("Come");
		yield return new WaitForSeconds(1.0f);
		medalMenu = Instantiate(medalsmenuLabel) as GameObject;
		playersMenu = Instantiate(playersmenuLabel) as GameObject;
	}
	
	void StartOptions()
	{
		animation.Play ("Back");
		optionsmenuLabel.animation.Play("Come");
	}
	
	void applyChoice ()
	{
		if (choice == Choice.E_PLAY)
			StartCoroutine(StartPlay());
		else if (choice == Choice.E_EDITOR)
			Fade.loadLevel("EditorMenu", fadeOutTime, 1, fadeColor);
		else if (choice == Choice.E_OPTIONS)
			StartOptions();
		else if (choice == Choice.E_CREDITS)
			Fade.loadLevel("Credits", fadeOutTime, 1, fadeColor);
		else if (choice == Choice.E_QUIT)
			Application.Quit();
	}
	
	void checkClicks ()
	{
		if (play.isClicked)
		{
			play.resetClick();
			choice = Choice.E_PLAY;
			applyChoice();
		}
		else if (editor.isClicked)
		{
			editor.resetClick();
			choice = Choice.E_EDITOR;
			applyChoice();
		}
		else if (options.isClicked)
		{
			options.resetClick();
			choice = Choice.E_OPTIONS;
			applyChoice();
		}
		else if (credits.isClicked)
		{
			credits.resetClick();
			choice = Choice.E_CREDITS;
			applyChoice();
		}
		else if (quit.isClicked)
		{
			quit.resetClick();
			choice = Choice.E_QUIT;
			applyChoice();
		}
	}
	
	void Awake ()
	{
		play = playLabel.GetComponent<Button>();
		editor = editorLabel.GetComponent<Button>();
		options = optionsLabel.GetComponent<Button>();
		credits = creditsLabel.GetComponent<Button>();
		quit = quitLabel.GetComponent<Button>();
		if (!play || !editor || !options || !credits || !quit)
			return;
	}
	
	void Update ()
	{
		checkClicks();
	}
}
