﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {
	
	public float delayTime = 0.5f;
	
	public Color fadeColor;
	float fadeOutTime = 3.0f;
	
	void Update ()
	{
		delayTime -= Time.deltaTime;
		if (delayTime < 0)
		{
			renderer.enabled = !renderer.enabled;
			delayTime = 0.5f;
		}
		if (Input.GetKeyDown(KeyCode.Space) ||
			Input.GetKeyDown(KeyCode.JoystickButton7))
			Fade.loadLevel("MainMenu", fadeOutTime, 1, fadeColor);
	}
}
