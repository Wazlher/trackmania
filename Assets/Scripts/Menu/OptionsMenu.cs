﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour {

	private enum Choice
	{
		E_RIGHTVOL,
		E_LEFTVOL,
		E_RIGHTSCR,
		E_LEFTSCR,
		E_BACK
	}
	
	int volume = 10;
	bool fullscreen;
	
	Choice choice;
	
	Button rightscr = null;
	Button leftscr = null;
	Button rightvol = null;
	Button leftvol = null;
	Button back = null; 
	
	public GameObject mainmenuLabel;
	
	public GameObject valuescrLabel;
	public GameObject valuevolLabel;
	
	public GameObject rightscrLabel;
	public GameObject leftscrLabel;
	public GameObject rightvolLabel;
	public GameObject leftvolLabel;
	public GameObject backLabel;
	
	void animMenu ()
	{
		animation.Play ("Back");
		mainmenuLabel.animation.Play ("Come");
	}
	
	void changePercentLabel(GameObject label, int percent)
	{
		TextMesh text = label.GetComponent<TextMesh>();
		text.text = percent.ToString();
	}
	
	void changeBooleanLabel(GameObject label, bool values)
	{
		TextMesh text = label.GetComponent<TextMesh>();
		if (values)
			text.text = "Fullscreen";
		else
			text.text = "Windowed";
	}
	
	void applyChoice ()
	{
		if (choice == Choice.E_RIGHTSCR)
		{
			fullscreen = !fullscreen;
			Screen.fullScreen = fullscreen;
			changeBooleanLabel(valuescrLabel, fullscreen);
		}
		else if (choice == Choice.E_LEFTSCR)
		{
			fullscreen = !fullscreen;
			Screen.fullScreen = fullscreen;
			changeBooleanLabel(valuescrLabel, fullscreen);
		}
		if (choice == Choice.E_RIGHTVOL)
		{
			if (volume != 10)
			{
				AudioListener.volume += 0.1f;
				volume++;
				changePercentLabel(valuevolLabel, volume);
			}
		}
		else if (choice == Choice.E_LEFTVOL)
		{
			if (volume != 0)
			{
				AudioListener.volume -= 0.1f;
				volume--;
				changePercentLabel(valuevolLabel, volume);
			}
		}
		else if (choice == Choice.E_BACK)
		{
			animMenu();
		}
	}
	
	void checkClicks ()
	{
		if (rightscr.isClicked)
		{
			rightscr.resetClick();
			choice = Choice.E_RIGHTSCR;
			applyChoice();
		}
		else if (leftscr.isClicked)
		{
			leftscr.resetClick();
			choice = Choice.E_LEFTSCR;
			applyChoice();
		}
		if (rightvol.isClicked)
		{
			rightvol.resetClick();
			choice = Choice.E_RIGHTVOL;
			applyChoice();
		}
		else if (leftvol.isClicked)
		{
			leftvol.resetClick();
			choice = Choice.E_LEFTVOL;
			applyChoice();
		}
		else if (back.isClicked)
		{
			back.resetClick();
			choice = Choice.E_BACK;
			applyChoice();
		}
	}
	
	void Awake ()
	{	
		fullscreen = Screen.fullScreen;
		
		changePercentLabel(valuevolLabel, volume);
		changeBooleanLabel(valuescrLabel, fullscreen);
		
		rightscr = rightscrLabel.GetComponent<Button>();
		leftscr = leftscrLabel.GetComponent<Button>();
		rightvol = rightvolLabel.GetComponent<Button>();
		leftvol = leftvolLabel.GetComponent<Button>();
		back = backLabel.GetComponent<Button>();
		if (!back || !leftscr || !rightscr || !leftvol || !rightvol)
			return;
	}
	
	void Update ()
	{
		checkClicks();
	}
}
