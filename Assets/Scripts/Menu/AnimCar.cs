﻿using UnityEngine;
using System.Collections;

public class AnimCar : MonoBehaviour {
	
	private	Vector3	rot;

	// Use this for initialization
	void Start () {
		rot = new Vector3(0.0f, 10.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(rot * Time.deltaTime);
	}
}
