﻿using System.IO;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EditorMenu : MonoBehaviour {

	private enum Choice
	{
		E_NEWTRACK,
		E_LOADTRACK,
		E_BACK
	}
	
	public Color fadeColor;
	float fadeOutTime = 1.0f;
	
	Choice choice;
	
	Button newTrack = null;
	Button loadTrack = null;
	Button back = null;
	
	public GameObject PrefabLoad;
	
	public GameObject newtrackLabel;
	public GameObject loadtrackLabel;
	public GameObject backLabel;
	
	GameObject Menu;
	bool isDeployed = false;
	
	public FileInfo[] filesArray;
	
	void CountTrack()
	{
		string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves");
		
		DirectoryInfo directory = new DirectoryInfo(path);
		filesArray = directory.GetFiles();
	}
	
	void DisplayMenu()
	{
		Menu = Instantiate(PrefabLoad) as GameObject;
	}
	
	void DeleteMenu()
	{
		Destroy(Menu);
	}
	
	void LoadTrack()
	{
		if (isDeployed == false)
		{
			CountTrack();
			if (filesArray.Length > 0)
			{
				back.transform.Translate(new Vector3(0.0f, -9.0f, 0.0f));
				DisplayMenu();
				isDeployed = true;
			}
		}
		else
		{
			DeleteMenu();
			back.transform.Translate(new Vector3(0.0f, 9.0f, 0.0f));
			isDeployed = false;
		}
	}
	
	void applyChoice ()
	{
		if (choice == Choice.E_NEWTRACK)
			Fade.loadLevel("Editor", fadeOutTime, 1, fadeColor);
		else if (choice == Choice.E_LOADTRACK)
			LoadTrack();
		else if (choice == Choice.E_BACK)
			Fade.loadLevel("MainMenu", fadeOutTime, 1, fadeColor);
	}
	
	void checkClicks ()
	{
		if (newTrack.isClicked)
		{
			newTrack.resetClick();
			choice = Choice.E_NEWTRACK;
			applyChoice();
		}
		else if (loadTrack.isClicked)
		{
			loadTrack.resetClick();
			choice = Choice.E_LOADTRACK;
			applyChoice();
		}
		else if (back.isClicked)
		{
			back.resetClick();
			choice = Choice.E_BACK;
			applyChoice();
		}
	}
	
	void Awake ()
	{
		newTrack = newtrackLabel.GetComponent<Button>();
		loadTrack = loadtrackLabel.GetComponent<Button>();
		back = backLabel.GetComponent<Button>();
		if (!newTrack || !loadTrack || !back)
			return;
	}
	
	void Update ()
	{
		checkClicks();
	}
}
