﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class PlayMenu : MonoBehaviour
{

    private enum Choice
    {
        E_START,
        E_RIGHT,
        E_LEFT,
        E_BACK
    }

    public Camera camera;

    public Color fadeColor;
    float fadeOutTime = 3.0f;

    int track = 1;
    int countTrack = 0;

    Choice choice = Choice.E_START;

    Button start = null;
    Button right = null;
    Button left = null;
    Button back = null;

    public GameObject[] tracks;
    GameObject currentTrack;

    public GameObject mainmenuLabel;

    public GameObject startLabel;
    public GameObject rightLabel;
    public GameObject leftLabel;
    public GameObject backLabel;

    FileInfo[] filesArray;

    WWW www;

    public IEnumerator WaitForPicture()
    {
        yield return www;
    }

    void animMenu()
    {
        animation.Play("Back");
        mainmenuLabel.animation.Play("Come");
        Destroy(GameObject.Find("MedalsMenu(Clone)"));
        Destroy(GameObject.Find("PlayersMenu(Clone)"));
        PlayersMenu.player = 1;
        track = 1;
    }

    void applyChoice()
    {
        if (choice == Choice.E_START)
        {
            if (track == 1)
            {
                if (PlayersMenu.player == 1)
                {
                    camera.animation.Play("AnimCam");
                    Fade.loadLevel("Terrain_19", fadeOutTime, 1, fadeColor);
                }
                else if (PlayersMenu.player == 2)
                {
                    camera.animation.Play("AnimCam");
                    PlayersMenu.player = 1;
                    Fade.loadLevel("Terrain_19_DUO", fadeOutTime, 1, fadeColor);
                }
            }
            else if (track == 2)
            {
                if (PlayersMenu.player == 1)
                {
                    camera.animation.Play("AnimCam");
                    Fade.loadLevel("Terrain_1", fadeOutTime, 1, fadeColor);
                }
                else if (PlayersMenu.player == 2)
                {
                    camera.animation.Play("AnimCam");
                    PlayersMenu.player = 1;
                    Fade.loadLevel("Terrain_1_DUO", fadeOutTime, 1, fadeColor);
                }
            }
            else if (track == 3)
            {
                if (PlayersMenu.player == 1)
                {
                    camera.animation.Play("AnimCam");
                    Fade.loadLevel("Terrain_15", fadeOutTime, 1, fadeColor);
                }
                else if (PlayersMenu.player == 2)
                {
                    camera.animation.Play("AnimCam");
                    PlayersMenu.player = 1;
                    Fade.loadLevel("Terrain_15_DUO", fadeOutTime, 1, fadeColor);
                }
            }
            else if (track > 3)
            {
                PersistantGame pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
                pg.levelName = filesArray[track - 4].Name;
                pg.levelName = pg.levelName.Remove(pg.levelName.Length - 4);
                Fade.loadLevel("Terrain_prefab", fadeOutTime, 1, fadeColor);
            }
        }
        else if (choice == Choice.E_RIGHT)
        {
            if (track != countTrack)
                track++;
            else
                track = 1;
            Destroy(currentTrack);
            if (track <= 3)
            {
                currentTrack = Instantiate(tracks[track]) as GameObject;
                currentTrack.transform.parent = transform;
            }
            else
            {
                FileInfo file = filesArray[track - 4];

                www = new WWW("file://" + file.FullName);
                StartCoroutine(WaitForPicture());

                currentTrack = Instantiate(tracks[4]) as GameObject;
                currentTrack.transform.parent = transform;

                currentTrack.renderer.material.mainTexture = www.texture;
            }
        }
        else if (choice == Choice.E_LEFT)
        {
            if (track != 1)
                track--;
            else
                track = countTrack;
            Destroy(currentTrack);
            if (track <= 3)
            {
                currentTrack = Instantiate(tracks[track]) as GameObject;
                currentTrack.transform.parent = transform;
            }
            else
            {
                FileInfo file = filesArray[track - 4];

                www = new WWW("file://" + file.FullName);
                StartCoroutine(WaitForPicture());

                currentTrack = Instantiate(tracks[4]) as GameObject;
                currentTrack.transform.parent = transform;

                currentTrack.renderer.material.mainTexture = www.texture;
            }
        }
        else if (choice == Choice.E_BACK)
        {
            animMenu();
        }
    }

    void checkClicks()
    {
        if (start.isClicked)
        {
            start.resetClick();
            choice = Choice.E_START;
            applyChoice();
        }
        else if (right.isClicked)
        {
            right.resetClick();
            choice = Choice.E_RIGHT;
            applyChoice();
        }
        else if (left.isClicked)
        {
            left.resetClick();
            choice = Choice.E_LEFT;
            applyChoice();
        }
        else if (back.isClicked)
        {
            back.resetClick();
            choice = Choice.E_BACK;
            applyChoice();
        }
    }

    void SetArray()
    {
        string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Images");

        DirectoryInfo directory = new DirectoryInfo(path);
        filesArray = directory.GetFiles();
    }

    void Awake()
    {
        SetArray();
        countTrack = 3 + filesArray.Length;

        start = startLabel.GetComponent<Button>();
        right = rightLabel.GetComponent<Button>();
        left = leftLabel.GetComponent<Button>();
        back = backLabel.GetComponent<Button>();
        if (!start || !back || !left || !right)
            return;

        currentTrack = Instantiate(tracks[0]) as GameObject;
        currentTrack.transform.parent = transform;
    }

    void Update()
    {
        checkClicks();
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.T))
            Fade.loadLevel("EndTrack", 0f, 0f, Color.black);
#endif
    }
}