﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
	
	public Material[] Picture;
	
	int selectedPicture;
	
	float time;
	
	// Use this for initialization
	void Start () {
		
		selectedPicture = Random.Range(0, Picture.Length - 1);
		renderer.material = Picture[selectedPicture];
		time = Time.time;
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Time.time - time > 10.0f)
		{
			time = Time.time;
			renderer.material = Picture[selectedPicture];
			if (selectedPicture == Picture.Length - 1)
				selectedPicture = 0;
			else
				selectedPicture++;
			

		}
		
	}
}
