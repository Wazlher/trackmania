﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour
{
	private static Fade	instance = null;
	private Material		material = null;
	private string			levelName = "";
	private int				levelIndex = 0;
	private bool			fading = false;
	
	/*	Instances	*/
	private static Fade Instance
	{
		get
		{
			if (instance == null)
				instance = (new GameObject("Fade")).AddComponent<Fade>();
			return instance;
		}
	}
	public static bool isFading
	{
		get { return Instance.fading;}
	}
	
	/*	Functions	*/
	
	private void	Awake()
	{
		DontDestroyOnLoad(this);
		instance = this;
		this.material = new Material("Shader \"Plane/No zTest\" { SubShader { Pass { Blend SrcAlpha OneMinusSrcAlpha ZWrite Off Cull Off Fog { Mode Off } BindChannels { Bind \"Color\",color } } } }");
	}
	
	private void	drawQuad(Color color, float alpha)
	{
		color.a = alpha;
		this.material.SetPass(0);
		GL.Color(color);
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.Begin(GL.QUADS);
		GL.Vertex3(0, 0, -1);
		GL.Vertex3(0, 1, -1);
		GL.Vertex3(1, 1, -1);
		GL.Vertex3(1, 0, -1);
		GL.End();
		GL.PopMatrix();
	}
	
	private IEnumerator	fade(float fadeOutTime, float fadeInTime, Color color)
	{
		float	time = 0.0f;
		
		while (time < 1.0f)
		{
			yield return new WaitForEndOfFrame();
			time = Mathf.Clamp01(time + Time.deltaTime / fadeOutTime);
			this.drawQuad(color, time);
		}
		if (this.levelName != "")
			Application.LoadLevel(this.levelName);
		else
			Application.LoadLevel(this.levelIndex);
		while (time > 0.0f)
		{
			yield return new WaitForEndOfFrame();
			time = Mathf.Clamp01(time - Time.deltaTime / fadeInTime);
			this.drawQuad(color, time);
		}
		this.fading = false;
	}
	
	private void	startFading(float fadeOutTime, float fadeInTime, Color color)
	{
		this.fading = true;
		this.StartCoroutine(this.fade(fadeOutTime, fadeInTime, color));
	}
	
	public static void	loadLevel(string levelName, float fadeOutTime, float fadeInTime, Color color)
	{
		if (isFading)
			return;
		instance.levelName = levelName;
		instance.startFading(fadeOutTime, fadeInTime, color);
	}
	
	public static void	loadLevel(int levelIndex, float fadeOutTime, float fadeInTime, Color color)
	{
		if (isFading)
			return;
		instance.levelName = "";
		instance.levelIndex = levelIndex;
		instance.startFading(fadeOutTime, fadeInTime, color);
	}
}
