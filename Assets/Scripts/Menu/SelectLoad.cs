﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class SelectLoad : MonoBehaviour {

	private enum Choice
	{
		E_LOAD,
		E_DELETE,
		E_RIGHT,
		E_LEFT
	}
	
	public Color fadeColor;
	float fadeOutTime = 3.0f;
	
	Choice choice;
	
	int selected = 0;
	
	Button load = null;
	Button delete = null;
	Button right = null;
	Button left = null;
	
	public GameObject loadLabel;
	public GameObject deleteLabel;
	public GameObject rightLabel;
	public GameObject leftLabel;
	
	public GameObject pictureLabel;
	
	FileInfo[] filesArray;
	FileInfo[] screensArray;
	
	WWW www;
	
	public IEnumerator WaitForPicture()
	{
		yield return www;
	}
	
	void Refresh()
	{
		FileInfo file = filesArray[selected];
		
		TextMesh text = load.GetComponent<TextMesh>();
		text.text = file.Name.Remove(file.Name.Length - 4, 4);
		
		file = screensArray[selected];
		
		www = new WWW("file://" + file.FullName);
		StartCoroutine(WaitForPicture());
		
		pictureLabel.renderer.material.mainTexture = www.texture;
	}
	
	void Delete()
	{
		FileInfo file = filesArray[selected];
		file.Delete();
		
		file = screensArray[selected];
		file.Delete();
	}
	
	void SetArray()
	{
		string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves");
		
		DirectoryInfo directory = new DirectoryInfo(path);
		filesArray = directory.GetFiles();
		
		path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Images");
		
		directory = new DirectoryInfo(path);
		screensArray = directory.GetFiles();
	}
	
	void applyChoice ()
	{
		if (choice == Choice.E_LOAD)
		{
            PersistantGame pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
            pg.levelName = filesArray[selected].Name;
            pg.levelName = pg.levelName.Remove(pg.levelName.Length - 4);
            Fade.loadLevel("Editor", 1.0f, 1.0f, Color.black);
		}
		else if (choice == Choice.E_DELETE)
		{
			Delete();
			selected = 0;
			init();
		}
		else if (choice == Choice.E_RIGHT)
		{
			if (selected != filesArray.Length - 1)
			{
				selected++;
				Refresh();
			}
			else
			{
				selected = 0;
				Refresh();
			}
		}
		else if (choice == Choice.E_LEFT)
		{
			if (selected != 0)
			{
				selected--;
				Refresh();
			}
			else
			{
				selected = filesArray.Length - 1;
				Refresh();
			}
		}
	}
	
	void checkClicks ()
	{
		if (load.isClicked)
		{
			load.resetClick();
			choice = Choice.E_LOAD;
			applyChoice();
		}
		else if (delete.isClicked)
		{
			delete.resetClick();
			choice = Choice.E_DELETE;
			applyChoice();
		}
		else if (right.isClicked)
		{
			right.resetClick();
			choice = Choice.E_RIGHT;
			applyChoice();
		}
		else if (left.isClicked)
		{
			left.resetClick();
			choice = Choice.E_LEFT;
			applyChoice();
		}
	}
	
	void init()
	{
		SetArray();
		
		if (filesArray.Length > 0)
		{
			FileInfo file = filesArray[selected];
			TextMesh text = load.GetComponent<TextMesh>();
			text.text = file.Name.Remove(file.Name.Length - 4, 4);
		
			file = screensArray[selected];
			www = new WWW("file://" + file.FullName);
			StartCoroutine(WaitForPicture());
			pictureLabel.renderer.material.mainTexture = www.texture;
		}
		else
			Application.LoadLevel("EditorMenu");
	}
	
	void Awake ()
	{
		load = loadLabel.GetComponent<Button>();
		delete = deleteLabel.GetComponent<Button>();
		right = rightLabel.GetComponent<Button>();
		left = leftLabel.GetComponent<Button>();
		if (!load || !delete || !left || !right)
			return;
		init();
	}
	
	void Update ()
	{
		checkClicks();
	}
}
