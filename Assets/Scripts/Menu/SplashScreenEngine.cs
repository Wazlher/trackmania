﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class SplashScreenEngine : MonoBehaviour
{

    public Color fadeColor;
    public float delayTime = 1.0f;
    float fadeOutTime = 3.0f;

    void Start()
    {
        if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/")))
            Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/"));
        if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/")))
            Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Saves/"));
        if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Images/")))
            Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trackmania/Images/"));
    }

    void Update()
    {
        delayTime -= Time.deltaTime;
        if (!Fade.isFading &&
            (Input.GetKeyDown(KeyCode.Space) ||
            Input.GetKeyDown(KeyCode.Escape)))
            Application.LoadLevel("SplashScreen");
        if (delayTime < 0)
            Fade.loadLevel("SplashScreen", fadeOutTime, 1, fadeColor);
    }
}
