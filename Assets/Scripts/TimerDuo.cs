﻿using UnityEngine;
using System.Collections;

public class TimerDuo : MonoBehaviour {
    public float timeLeft;
	public string textTime;

	
    public void Update()
    {
        timeLeft -= Time.deltaTime;
  	    if (timeLeft <= 0.0f)
        {
            print("Finish");
        }
        else
        {
            print("Go");
        }
    }
	
	void OnGUI()
	{
		int minutes = (int)timeLeft / 60;
		int seconds = (int)timeLeft % 60;
		
		GUIStyle duoStyle = new GUIStyle(GUI.skin.label);
		duoStyle.font = (Font)Resources.Load("Font/virgo");
		textTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		GUI.Label (new Rect ((Screen.width/2) - 90, 20, 200, 70), textTime, duoStyle);
	}
}
