using UnityEngine;
using System.Collections;

public class CarCamera : MonoBehaviour
{
    public Transform target = null;
    public float height = 1f;
    public float positionDamping = 3f;
    public float velocityDamping = 3f;
    public float distance = 4f;
    public LayerMask ignoreLayers = -1;
    public float offset = 2;
    public float smooth = 1.0f;


    private RaycastHit hit = new RaycastHit();

    private Vector3 prevVelocity = Vector3.zero;
    private LayerMask raycastLayers = -1;

    private Vector3 currentVelocity = Vector3.zero;

    void Start()
    {
        raycastLayers = ~ignoreLayers;
    }

    void FixedUpdate()
    {
        currentVelocity = Vector3.Lerp(prevVelocity, target.root.rigidbody.velocity, velocityDamping * Time.deltaTime);
        currentVelocity.y = 0;
        prevVelocity = currentVelocity;
    }

    void LateUpdate()
    {
        float speedFactor = Mathf.Clamp01(target.root.rigidbody.velocity.magnitude / 50.0f);
        camera.fieldOfView = Mathf.Lerp(55, 75, speedFactor);
        float currentDistance = Mathf.Lerp(5.5f, 7.0f, speedFactor);

        currentVelocity = currentVelocity.normalized;

        Vector3 newTargetPosition = target.position + Vector3.up * height;
        //Debug.Log(currentVelocity);
        Vector3 newPosition = newTargetPosition - (currentVelocity * currentDistance * smooth);
        newPosition.y = newTargetPosition.y + offset;

        /* Check de collision entre car & camera */
        if (speedFactor < 0.0001f)
            newPosition = transform.position;
        Vector3 targetDirection = newPosition - newTargetPosition;
        if (Physics.Raycast(newTargetPosition, targetDirection, out hit, currentDistance, raycastLayers))
            newPosition = hit.point;

        transform.position = newPosition;
        transform.LookAt(newTargetPosition);
    }
}
