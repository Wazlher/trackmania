﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class CameraSwap : MonoBehaviour {

    public Camera thirdPerson;
    public Camera firstPerson;
    private bool first;
    private bool canSwitch;
	private bool canSwitchPad;
	public static bool onLooping;
	public int player;
	public int nbPlayer;

	// Use this for initialization
    void Awake()
    {
        canSwitch = true;
		canSwitchPad = true;
        thirdPerson.enabled = true;
        firstPerson.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		if (CameraLooping.onLooping1 == false && CameraLooping.onLooping2 == false)
		{
			GamePadState state;

			if (player == 2 || nbPlayer == 1)
				state = GamePad.GetState((PlayerIndex)(0));
			else
				state = GamePad.GetState((PlayerIndex)(1));

			if (player == 2)
			{
				if ((state.IsConnected && state.Buttons.Y == ButtonState.Pressed) && canSwitchPad)
				{
					thirdPerson.enabled = !thirdPerson.enabled;
					firstPerson.enabled = !firstPerson.enabled;
					canSwitchPad = false;
				}
				else if (state.IsConnected && state.Buttons.Y != ButtonState.Pressed)
					canSwitchPad = true;
			}
			else
			{
				if (Input.GetAxis("SwitchCam") > 0.0f && canSwitch)
				{
					thirdPerson.enabled = !thirdPerson.enabled;
					firstPerson.enabled = !firstPerson.enabled;
					canSwitch = false;
				}
				else if (Input.GetAxis("SwitchCam") == 0.0f)
					canSwitch = true;
				if (state.IsConnected)
				{
					if (state.Buttons.Y == ButtonState.Pressed && canSwitchPad)
					{
						thirdPerson.enabled = !thirdPerson.enabled;
						firstPerson.enabled = !firstPerson.enabled;
						canSwitchPad = false;
					}
					else if (state.Buttons.Y != ButtonState.Pressed)
						canSwitchPad = true;
				}
			}
		}
	}
}
