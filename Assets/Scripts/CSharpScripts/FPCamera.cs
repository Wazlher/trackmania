﻿using UnityEngine;
using System.Collections;

public class FPCamera : MonoBehaviour {

    public Transform car;

	void Update ()
    {
        this.transform.position = car.transform.position;
        this.transform.rotation = car.transform.rotation;
        this.transform.position += car.transform.forward * 0.85f;
        this.transform.position += car.transform.up * 1.0f;
    }
}