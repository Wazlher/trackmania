﻿using UnityEngine;
using System.Collections;

public class CameraLooping : MonoBehaviour
{
	public bool Switch;

    public Camera thirdPerson1;
    public Camera firstPerson1;

    public Camera thirdPerson2;
    public Camera firstPerson2;

    public static bool onLooping1 = false;
    public static bool onLooping2 = false;

    void OnTriggerEnter(Collider c)
    {
        if (c.name == "Collider1")
        {
            if (Switch)
			{
				thirdPerson1.enabled = false;
				firstPerson1.enabled = true;
				onLooping1 = true;
			}
			else
			{
				thirdPerson1.enabled = true;
				firstPerson1.enabled = false;
				onLooping1 = false;
			}
        }
        else if (c.name == "Collider2")
        {
            if (Switch)
			{
				thirdPerson1.enabled = false;
				firstPerson1.enabled = true;
				onLooping1 = true;
			}
			else
			{
				thirdPerson1.enabled = true;
				firstPerson1.enabled = false;
				onLooping1 = false;
			}
        }
    }
}
