﻿using UnityEngine;
using System.Collections;

public class SkiddingScript : MonoBehaviour
{	
	public float skidAt = 1.5f;
	private float currentFriactionValue;
	private int skidding;
	private Vector3[] lastPos = new Vector3[2];
	public float markWidth = 0.2f;
	public Material SkidMaterial;
	
	void  Start ()
	{
	}
	
	void  Update ()
	{
		WheelHit hit;
		hit = new WheelHit();
		transform.GetComponent<WheelCollider>().GetGroundHit(out hit);
		currentFriactionValue = Mathf.Abs(hit.sidewaysSlip);
		
		if(skidAt <= currentFriactionValue)
			SkidMesh();
		else
			skidding = 0;
	}
	
	void SkidMesh()
	{
		WheelHit hit;
		hit = new WheelHit();
		transform.GetComponent<WheelCollider>().GetGroundHit(out hit);
		GameObject mark = new GameObject("Mark");
		MeshFilter filter = mark.AddComponent<MeshFilter>();
		mark.AddComponent<MeshRenderer>();
		Mesh markMesh = new Mesh();
		Vector3[] verticies = new Vector3[4];
		if (skidding == 0)
		{
			verticies[0] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(markWidth, 0.01f, 0.0f);
			verticies[1] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(-markWidth, 0.01f, 0.0f);
			verticies[2] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(-markWidth, 0.01f, 0.0f);
			verticies[3] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(markWidth, 0.01f, 0.0f);
			lastPos[0] = verticies[2];
			lastPos[1] = verticies[3];
			skidding = 1;
		}
		else
		{
			verticies[1] = lastPos[0];
			verticies[0] = lastPos[1];
			verticies[2] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(-markWidth, 0.01f, 0.0f);
			verticies[3] = hit.point + Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3(markWidth, 0.01f, 0.0f);
			lastPos[0] = verticies[2];
			lastPos[1] = verticies[3];
		}
		markMesh.vertices = verticies;
		markMesh.triangles = new int[] {0, 1, 2, 2, 3, 0};
		markMesh.RecalculateNormals();
		Vector2[] uvm = new Vector2[4];
		uvm[0] = new Vector2(1, 0);
		uvm[1] = new Vector2(0, 0);
		uvm[2] = new Vector2(0, 1);
		uvm[3] = new Vector2(1, 1);
		markMesh.uv = uvm;
		filter.mesh = markMesh;
		mark.renderer.material = SkidMaterial;
		//mark.AddComponent(DestroyTimerScript);
	}
}