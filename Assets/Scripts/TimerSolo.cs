﻿using UnityEngine;
using System.Collections;

public class TimerSolo : MonoBehaviour {

	private float startTime;
	public string textTime;
	
	
	void Awake()
	{
		startTime = Time.time;
	}
	
	void OnGUI()
	{
		float guiTime = Time.time - startTime - 6.0f;
		
		int minutes = (int)guiTime / 60;
		int seconds = (int)guiTime % 60;
		
		GUIStyle soloStyle = new GUIStyle(GUI.skin.label);
		soloStyle.font = (Font)Resources.Load("Font/virgo");
		textTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		GUI.Label (new Rect ((Screen.width/2) - 90, 20, 200, 70), textTime, soloStyle);
	}
}
