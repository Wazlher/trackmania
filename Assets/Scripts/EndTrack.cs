﻿using UnityEngine;
using System.Collections;

public class EndTrack : MonoBehaviour {

    int nbPlayer;
    string timeFirst;
    string timeSecond;
    PersistantGame gs;
    int winner;

    public TextMesh guiTextSolo;
    public TextMesh guiTextFirst;
    public TextMesh guiTextScd;

    public TextMesh goldText;
    public TextMesh silverText;
    public TextMesh bronzeText;

    public Transform carOne;
    public Transform carTwo;

    public GameObject solo;
    public GameObject duo;

	// Use this for initialization
	void Start () {
        gs = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
        nbPlayer = gs.nbPlayer;
        timeFirst = gs.timeFirst;
        timeSecond = gs.timeSecond;
        winner = gs.winner;
        bronzeText.text = gs.bronzeTime;
        silverText.text = gs.silverTime;
        goldText.text = gs.goldTime;

        if (nbPlayer == 1)
            SetSoloGUI();
        else
            SetDuoGUI();
	}

    void SetSoloGUI()
    {
        guiTextSolo.text = timeFirst;
        duo.active = false;
        if (CheckTwoTimeStrings(timeFirst, gs.bronzeTime))
            bronzeText.color = Color.magenta;
        if (CheckTwoTimeStrings(timeFirst, gs.silverTime))
            silverText.color = Color.magenta;
        if (CheckTwoTimeStrings(timeFirst, gs.goldTime))
            goldText.color = Color.magenta;
    }

    void SetDuoGUI()
    {
        solo.active = false;
        if (winner == 1)
        {
            guiTextFirst.text = timeFirst;
            guiTextScd.text = timeSecond;
        }
        else
        {
            Vector3 vec = carOne.transform.position;
            Quaternion quat = carOne.transform.rotation;

            carOne.transform.position = carTwo.transform.position;
            carOne.transform.rotation = carTwo.transform.rotation;
            carTwo.transform.position = vec;
            carTwo.transform.rotation = quat;

            guiTextFirst.text = timeSecond;
            guiTextScd.text = timeFirst;
        }
    }

    bool CheckTwoTimeStrings(string one, string two)
    {
        string three = one.Remove(2, 3), four = two.Remove(2, 3), five = one.Substring(3, 2), six = two.Substring(3, 2);

        int minute1 = int.Parse(three);
        int minute2 = int.Parse(four);
        int second1 = int.Parse(five);
        int second2 = int.Parse(six);

        Debug.Log("MyScore = " + minute1 + " : " + second1 + " Medal score = " + minute2 + " : " + second2);
        if (minute1 > minute2)
            return false;
        else if (minute1 < minute2)
            return true;
        else if (second1 > second2)
            return false;
        return true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            Fade.loadLevel("MainMenu", 0.5f, 0.5f, Color.black);
    }
}
