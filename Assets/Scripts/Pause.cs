﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Pause : MonoBehaviour {

	private bool paused;
	private int i;
	private bool stopCursor;
	private bool stopChange;
	private string[] menuText;
	
	private int cursorMenu;
	private bool option;
	
	public bool quitGame;
	
	private void InitTextMenu()
	{
		menuText = new string[4];
		menuText[0] = "Continue";
		menuText[1] = "Reset";
		menuText[2] = "Options";
		menuText[3] = "Quit";
	}
	
	private void SetText(string[] text)
	{
		i = 0;
		while (i < text.Length)
		{
			i++;
		}
	}
	
	void Start()
	{
		paused = false;
		InitTextMenu();
		cursorMenu = 0;
		stopCursor = false;
		stopChange = false;
		option = false;
		i = 0;
	}
	
	void GUIMenu(float originWidth, float originHeight)
	{
		Rect rect;
		bool[] menuButton;
		menuButton = new bool[menuText.Length];
		if (paused)
		{
			rect = new Rect(20.0f, 10.0f, originWidth - 40.0f, originHeight - 20.0f);
			GUI.Box(rect, "PAUSE");
			int count = 0;
			while (count < menuText.Length)
			{
				rect = new Rect(300.0f, 100.0f + (150.0f * count), 680.0f, 100.0f);
				GUI.SetNextControlName(menuText[count]);
				menuButton[count] = GUI.Button(rect, menuText[count]);
				if (stopChange)
					menuButton[count] = false;
				count++;
			}
			if (Input.GetAxisRaw("MovePause") == -1 && !stopCursor)
			{
				cursorMenu = (cursorMenu + 1) % menuText.Length;
				stopCursor = true;
			}
			if (Input.GetAxisRaw("MovePause") == 1 && !stopCursor)
			{
				cursorMenu = (cursorMenu + menuText.Length - 1) % menuText.Length;
				stopCursor = true;
			}
			if (Input.GetAxisRaw("MovePause") == 0 && stopCursor)
			{
				stopCursor = false;
			}
			GUI.FocusControl(menuText[cursorMenu]);
			if (Input.GetAxisRaw("ValidatePause") == 1 && !stopChange)
			{
				menuButton[cursorMenu] = true;
				stopChange = true;
			}
			if (menuButton[0] || (Input.GetAxisRaw("ValidatePause") == -1 && !stopChange))
			{
				Time.timeScale = 1;
				paused=false;
				cursorMenu = 0;
			}
			else
			{
				if (Input.GetAxisRaw("ValidatePause") == 0 && stopChange)
				{
					stopChange = false;
				}
				if (menuButton[2])
				{
					option = true;
				}
				else
				{
					if (menuButton[1])
					{
						Time.timeScale = 1;
						Application.LoadLevel(Application.loadedLevel);
					}
					if (menuButton[3])
					{
						if (quitGame)
						{
							Application.Quit();
						}
						else
						{
							Time.timeScale = 1;
							Application.LoadLevel("MainMenu");
						}
					}
				}
			}
		}
	}
	
	void GUIOption(float originWidth, float originHeight)
	{
		Rect rect;
		if (paused)
		{
			rect = new Rect(20.0f, 10.0f, originWidth - 40.0f, originHeight - 20.0f);
			GUI.Box(rect, "OPTIONS");
			rect = new Rect(300.0f, 100.0f, 680.0f, 100.0f);
			int vol = (int)(AudioListener.volume * 100);
			GUI.Label(rect, "Volume : " + vol.ToString() );
			rect = new Rect(300.0f, 250.0f, 680.0f, 100.0f);
			AudioListener.volume = GUI.HorizontalSlider(rect, AudioListener.volume, 0.0f, 1.0f);
			rect = new Rect(300.0f, 400.0f, 680.0f, 100.0f);
			GUI.SetNextControlName("Return");
			GUI.FocusControl("Return");
			if ((GUI.Button(rect, "Return") || Input.GetAxisRaw("ValidatePause") != 0) && !stopChange)
			{
				stopChange = true;
				option = false;
			}
			if (Input.GetAxisRaw("ValidatePause") == 0 && stopChange)
			{
				stopChange = false;
			}
			if (Input.GetAxisRaw("Horizontal") == -1)
			{
				AudioListener.volume -= 0.01f;
				if (AudioListener.volume < 0.0f)
					AudioListener.volume = 0.0f;
			}
			if (Input.GetAxisRaw("Horizontal") == 1)
			{
				AudioListener.volume += 0.01f;
				if (AudioListener.volume > 1.0f)
					AudioListener.volume = 1.0f;
			}
		}
	}
	
	void OnGUI()
	{
		GUI.skin.box.fontSize = 60;
		GUI.skin.label.fontSize = 60;
		GUI.skin.button.fontSize = 40;
		float originWidth = 1280;
		float originHeight = 720;
		Vector3 scale;
		scale.x = Screen.width/originWidth;
		scale.y = Screen.height/originHeight;
		scale.z = 1;
		Matrix4x4 svMat = GUI.matrix;
		GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);
		if (option)
			GUIOption(originWidth, originHeight);
		else
			GUIMenu(originWidth, originHeight);
		GUI.matrix = svMat;
	}
	
	public bool GamePaused()
	{
		return paused;
	}
	
	void Update ()
	{
        GamePadState state = GamePad.GetState((PlayerIndex)0);

		if ((state.IsConnected && state.Buttons.Start == ButtonState.Pressed) || (Input.GetAxisRaw("ActivatePause") != 0 && Input.GetButtonDown("ActivatePause")))
		{
			if(!paused)
			{
				Time.timeScale = 0;
				paused=true;
			}
			else
			{
				Time.timeScale = 1;
				paused=false;
				cursorMenu = 0;
				option = false;
			}
		}
	}
}