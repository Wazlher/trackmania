﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SaveGhost : MonoBehaviour {

	public GameObject listCheckpoint;
	public bool saveEnable;
	public string nameFileGhost;

	private List<DataGhost> listStatus;
	private checkpoint lastCheckpoint;

	// Use this for initialization
	void Start()
	{
		Debug.LogError("start");
		listStatus = new List<DataGhost>();
	}
	
	void FixedUpdate()
	{
		if (saveEnable == true)
		{
			listStatus.Add(new DataGhost(transform.position, transform.rotation));
			if (EndCourse())
			{
				saveEnable = false;
				SaveInFile();
			}
		}
	}

	bool EndCourse()
	{
		Debug.LogError("start test");
		GameObject g = GameObject.FindGameObjectWithTag("StartAndFinish");
		if (g == null)
			g = GameObject.FindGameObjectWithTag("Finish");
		return g.GetComponent<End>().EndRun();
	}

	void SaveInFile()
	{
		Debug.Log("Save");
		int count = listStatus.Count;
		int i = 0;
		try
		{
			using (BinaryWriter writer = new BinaryWriter(File.Open(nameFileGhost, FileMode.Create, FileAccess.Write)))
			{
				writer.Write(count);
				while (i < count)
				{
					writer.Write(listStatus[i].pos.x);
					writer.Write(listStatus[i].pos.y);
					writer.Write(listStatus[i].pos.z);
					writer.Write(listStatus[i].rot.x);
					writer.Write(listStatus[i].rot.y);
					writer.Write(listStatus[i].rot.z);
					writer.Write(listStatus[i].rot.w);
					i++;
				}
			}
		}
		catch
		{
		}
	}
}
