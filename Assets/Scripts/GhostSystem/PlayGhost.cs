﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class PlayGhost : MonoBehaviour {

	public GameObject ghostPrefab;
	public string file;

	private bool haveGhost;
	private List<DataGhost> listStatus;
	private GameObject car;
	private int frame;
	private int nbFrame;

	// Use this for initialization
	void Start()
	{
		listStatus = new List<DataGhost>();
		try
		{
			using (BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open, FileAccess.Read)))
			{
				haveGhost = true;
				frame = 0;
				int i = 0;
				nbFrame = reader.ReadInt32();
				while (i < nbFrame)
				{
					listStatus.Add(new DataGhost(new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()),
												 new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle())));
					i++;
				}
				car = Instantiate(ghostPrefab) as GameObject;
			}
		}
		catch
		{
			haveGhost = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
		if (haveGhost)
		{
			if (frame < nbFrame)
			{
				car.transform.position = listStatus[frame].pos;
				car.transform.rotation = listStatus[frame].rot;
				frame++;
			}
		}
	}
}
