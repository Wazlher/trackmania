﻿using UnityEngine;
using System.Collections;

public class DataGhost
{
	public Vector3 pos;
	public Quaternion rot;

	public DataGhost(Vector3 position, Quaternion rotation)
	{
		pos = new Vector3(position.x, position.y, position.z);
		rot = new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
	}
}
