﻿using UnityEngine;
using System.Collections;

public class TimerStartSolo : MonoBehaviour {	
	private float startTime;
	private string startPrint;

	private PlayersCar_C player;
	private CameraSwap cameraSwap;
	private PlayGhost playGhost;
	private SaveGhost saveGhost;
	private TimerSolo timer;
	private TimerStartSolo start;
	

	public void Start()
	{
		GameObject car = GameObject.Find("CAR");
		
		player = car.GetComponent<PlayersCar_C>();
		cameraSwap = car.GetComponent<CameraSwap>();
		playGhost = car.GetComponent<PlayGhost>();
		saveGhost = car.GetComponent<SaveGhost>();
		timer = car.GetComponent<TimerSolo>();
		start = car.GetComponent<TimerStartSolo>();
		player.enabled = false;
		cameraSwap.enabled = false;
		playGhost.enabled = false;
		saveGhost.enabled = false;
		timer.enabled = false;
	}

	void Awake()
	{
		startTime = Time.time;
	}
	
	void OnGUI()
	{
		float guiTime = Time.time - startTime;
		int seconds = (int)guiTime % 60;
		
		if (seconds <= 0)
			startPrint = "5";
		else if (seconds <= 1)
			startPrint = "4";
		else if (seconds <= 2)
			startPrint = "3";
		else if (seconds <= 3)
			startPrint = "2";
		else if (seconds <= 4)
			startPrint = "1";
		else if (seconds <= 5)
			startPrint = "GO !";
		else if (seconds > 3)
			startPrint = "";
		
		GUIStyle startStyle = new GUIStyle(GUI.skin.label);
		startStyle.fontSize = 200;
		startStyle.normal.textColor = Color.red;
		startStyle.font = (Font)Resources.Load("Font/virgo");
		if (startPrint == "GO !")
			GUI.Label (new Rect ((Screen.width/2) - 210, (Screen.height/2) - 100, 500, 200), startPrint, startStyle);
		else
			GUI.Label (new Rect ((Screen.width/2) - 50, (Screen.height/2) - 100, 500, 200), startPrint, startStyle);
	}
	
	void Update()
	{
		if (startPrint == "")
		{
			player.enabled = true;
			cameraSwap.enabled = true;
			playGhost.enabled = true;
			saveGhost.enabled = true;
			timer.enabled = true;
			start.enabled = false;
		}
	}
}
