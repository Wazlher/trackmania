﻿using UnityEngine;
using System.Collections;

public class UserTrack : MonoBehaviour {

    PersistantGame pg;
    Texture2D back;

	void Start()
    {
        pg = GameObject.FindGameObjectWithTag("PersistantGame").GetComponent<PersistantGame>();
        back = Resources.Load("Editeur/Return", typeof(Texture2D)) as Texture2D;
	}

    void OnGUI()
    {
        if (pg.editorMode)
        {
            if (GUI.Button(new Rect(5, 5, 35, 35), back))
                Fade.loadLevel("Editor", 0.5f, 0.5f, Color.black);
        }
    }
	
}
