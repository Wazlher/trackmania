﻿using UnityEngine;
using System.Collections;

public class checkpoint : MonoBehaviour {
		
	bool isActive;
	bool isActive2;
	private GameObject[] checkPoint;
	private CameraSwap tmp;
	
	void	OnTriggerEnter(Collider c)
	{
		tmp = (CameraSwap)c.transform.parent.transform.parent.GetComponent<CameraSwap>() as CameraSwap;
		if (tmp.player == 1)
		{
			for (int i = 0; i < checkPoint.Length; i++)
				checkPoint[i].GetComponent<checkpoint>().isActive = false;
			isActive = true;
		}
		else
		{
			for (int i = 0; i < checkPoint.Length; i++)
				checkPoint[i].GetComponent<checkpoint>().isActive2 = false;
			isActive2 = true;
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		checkPoint = GameObject.FindGameObjectsWithTag("Checkpoint");
		isActive = false;
		isActive2 = false;
	}
	
	public bool getIsActive()
	{
		return isActive;	
	}

	public bool getIsActive2()
	{
		return isActive2;
	}
}
