﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class inputCheckPoint : MonoBehaviour {
	
	public Transform 	player;
	public Transform	player2;
	public PlayersCar_C Car;
	public PlayersCar_C Car2;
	public int nbPlayer;

	private Vector3		saveVelocity;
	private Vector3		saveAngularVelocity;
	private	Quaternion	saveRot;
	private bool		comeback = false;
	private	Vector3 	velocity = new Vector3(0.0f, 0.0f, 0.0f);
	private Quaternion 	rot;
	private GameObject[] checkPoint;
	private int Player;
	
	// Use this for initialization
	void Start () 
	{
		checkPoint = GameObject.FindGameObjectsWithTag("Checkpoint");
	}
	
	// Update is called once per frame
	void Update () {

		GamePadState state = GamePad.GetState((PlayerIndex)(0));
		GamePadState state2 = GamePad.GetState((PlayerIndex)(1));

		if (nbPlayer == 1)
		{
			if ((state.IsConnected && state.Buttons.B == ButtonState.Pressed) || Input.GetAxis("CheckPoint") == 1)
			{
				checkLastPoint();
			}
			if (comeback == true)
			{
				Car.rigidbody.freezeRotation = false;
				comeback = false;
			}
		}
		else
		{
			if ((state2.IsConnected && state2.Buttons.B == ButtonState.Pressed) || Input.GetAxis("CheckPoint") == 1)
			{
				Debug.Log("lol");
				checkLastPoint();
			}
			if (comeback == true)
			{
				Car.rigidbody.freezeRotation = false;
				comeback = false;
				return;
			}
			if (state.IsConnected && state.Buttons.B == ButtonState.Pressed)
			{
				checkLastPointCar2();
			}
			if (comeback == true)
			{
				Car2.rigidbody.freezeRotation = false;
				comeback = false;
			}
		}
	}
	
	void	checkLastPoint()
	{
		for (int i = 0; i < checkPoint.Length; i++)
		{
			if (checkPoint[i].GetComponent<checkpoint>().getIsActive() == true)
			{
				rot = new Quaternion(0.0f, checkPoint[i].transform.rotation.y, 0.0f, 0.0f);
				saveAngularVelocity = Car.rigidbody.angularVelocity;
				saveVelocity = Car.rigidbody.velocity;
				saveRot = Car.rigidbody.rotation;

				player.transform.position = checkPoint[i].transform.position;
				player.transform.rotation = rot;

				Car.setStartValue();
				Car.rigidbody.freezeRotation = true;
				Car.rigidbody.angularVelocity = velocity;
				Car.rigidbody.velocity = velocity;
				comeback = true;
			}
		}
	}

	void checkLastPointCar2()
	{
		for (int i = 0; i < checkPoint.Length; i++)
		{
			if (checkPoint[i].GetComponent<checkpoint>().getIsActive2() == true)
			{
				rot = new Quaternion(0.0f, checkPoint[i].transform.rotation.y, 0.0f, 0.0f);
				saveAngularVelocity = Car2.rigidbody.angularVelocity;
				saveVelocity = Car2.rigidbody.velocity;
				saveRot = Car2.rigidbody.rotation;

				player2.transform.position = checkPoint[i].transform.position;
				player2.transform.rotation = rot;

				Car2.setStartValue();
				Car2.rigidbody.freezeRotation = true;
				Car2.rigidbody.angularVelocity = velocity;
				Car2.rigidbody.velocity = velocity;
				comeback = true;
			}
		}
	}
}
