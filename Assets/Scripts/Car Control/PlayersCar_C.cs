﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class PlayersCar_C : MonoBehaviour {
public	WheelCollider FrontLeftWheel;
public WheelCollider FrontRightWheel;
public	WheelCollider BackLeftWheel;
public WheelCollider BackRightWheel;
public float decellerationSpeed;
public float currentSpeed;
public float topSpeed;
private bool braked = false;
public float maxBrakeTorque = 50;
	
public float markWidth = 0.2f;
public int skidding;

public int player;
public int nbPlayer;
	
private float mySidewayFriction;
private float myForwardFriction;
private float slipSidewayFriction;
private float slipForwardFriction;

public float EngineTorque;


	void  Start ()
	{
		myForwardFriction = BackRightWheel.forwardFriction.stiffness;
		mySidewayFriction = BackRightWheel.sidewaysFriction.stiffness;
		Debug.Log(myForwardFriction);
		Debug.Log(mySidewayFriction);
		slipForwardFriction = 0.05f;
		slipSidewayFriction = 0.12f;
	}

	void  FixedUpdate ()
	{
		HandBrake();
		Control();
	}
	
	void Update (){
		BackLight();
	}
		
	void  Control()
	{
		rigidbody.centerOfMass = new Vector3 (0, 0, 0);
		currentSpeed = 2*22/7 * FrontLeftWheel.radius * FrontLeftWheel.rpm*60/1000;
        float valueV = 0;
		GamePadState state;
		if (player == 2 || nbPlayer == 1)
			state = GamePad.GetState((PlayerIndex)(0));
		else
			state = GamePad.GetState((PlayerIndex)(1));

		if (state.IsConnected)
		{
			valueV = state.Triggers.Right;
			if (valueV == 0)
				valueV = -state.Triggers.Left;
		}
		if (valueV == 0 && player == 1)
			valueV = Input.GetAxis("Vertical");
        if (!braked)
        {
            FrontLeftWheel.motorTorque = EngineTorque * valueV;
            FrontRightWheel.motorTorque = EngineTorque * valueV;
        }
        if (valueV == 0)
        {
            FrontLeftWheel.brakeTorque = decellerationSpeed;
            FrontRightWheel.brakeTorque = decellerationSpeed;
        }
        else
        {
            FrontLeftWheel.brakeTorque = 0;
            FrontRightWheel.brakeTorque = 0;
        }
        float valueH = 0;
        if (state.IsConnected)
            valueH = state.ThumbSticks.Left.X;
		if (valueH == 0 && player == 1)
            valueH = Input.GetAxis("Horizontal");
        if (currentSpeed >= 0)
        {
            FrontLeftWheel.steerAngle = ((20 - (currentSpeed / (20 + (currentSpeed / 100))))) / 2.8f * valueH;
            FrontRightWheel.steerAngle = ((20 - (currentSpeed / (20 + (currentSpeed / 100))))) / 2.8f * valueH;
        }
        else
        {
            FrontLeftWheel.steerAngle = ((20 - (-currentSpeed / (20 + (-currentSpeed / 100))))) / 2.8f * valueH;
            FrontRightWheel.steerAngle = ((20 - (-currentSpeed / (20 + (-currentSpeed / 100))))) / 2.8f * valueH;
        }
    }

    public void setStartValue()
    {
        currentSpeed = 0;
    }

    public Texture2D speedoMeterDial;
    public Texture2D speedoMeterPointer;

    void OnGUI()
    {
		if (nbPlayer == 2)
		{
			if (player == 1)
			{
        		GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 575, 300, 150), speedoMeterDial);
        		float speedFactor = currentSpeed / topSpeed;
        		float rotationAngle;
        		if (currentSpeed >= 0)
        		    rotationAngle = Mathf.Lerp(0, 180, speedFactor);
        		else
        		    rotationAngle = Mathf.Lerp(0, 180, -speedFactor);
        		GUIUtility.RotateAroundPivot(rotationAngle, new Vector2(Screen.width - 150, Screen.height - 425));
        		GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 575, 300, 300), speedoMeterPointer);
			}
			else if (player == 2)
			{
				GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 175, 300, 150), speedoMeterDial);
        		float speedFactor = currentSpeed / topSpeed;
        		float rotationAngle;
        		if (currentSpeed >= 0)
        		    rotationAngle = Mathf.Lerp(0, 180, speedFactor);
        		else
        		    rotationAngle = Mathf.Lerp(0, 180, -speedFactor);
        		GUIUtility.RotateAroundPivot(rotationAngle, new Vector2(Screen.width - 150, Screen.height - 25));
        		GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 175, 300, 300), speedoMeterPointer);
			}
		}
		else
		{
			GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 175, 300, 150), speedoMeterDial);
        	float speedFactor = currentSpeed / topSpeed;
        	float rotationAngle;
        	if (currentSpeed >= 0)
        	    rotationAngle = Mathf.Lerp(0, 180, speedFactor);
        	else
        	    rotationAngle = Mathf.Lerp(0, 180, -speedFactor);
        	GUIUtility.RotateAroundPivot(rotationAngle, new Vector2(Screen.width - 150, Screen.height - 25));
        	GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 175, 300, 300), speedoMeterPointer);
		}
			
    }

    public float maxReverseSpeed = 50;
    public GameObject backLightObjectRight;
    public GameObject backLightObjectLeft;
    public Material idleLightMaterial;
    public Material brakeLightMaterial;
    public Material reverseLightMaterial;

    void BackLight()
    {
        float valueV = 0;
        GamePadState state = GamePad.GetState((PlayerIndex)0);

        if (state.IsConnected)
        {
            valueV = state.Triggers.Right;
            if (valueV == 0)
                valueV = -state.Triggers.Left;
        }
        if (valueV == 0 && player == 1)
            valueV = Input.GetAxis("Vertical");
		else if (valueV == 0 && player == 2)
			valueV = Input.GetAxis("Vertical2");
        if (currentSpeed > 0 && valueV < 0 && !braked)
        {
            backLightObjectLeft.renderer.material = brakeLightMaterial;
            backLightObjectRight.renderer.material = brakeLightMaterial;
        }
        else if (currentSpeed < 0 && valueV > 0 && !braked)
        {
            backLightObjectLeft.renderer.material = brakeLightMaterial;
            backLightObjectRight.renderer.material = brakeLightMaterial;
        }
        else if (currentSpeed < 0 && valueV < 0 && !braked)
        {
            backLightObjectLeft.renderer.material = reverseLightMaterial;
            backLightObjectRight.renderer.material = reverseLightMaterial;
        }
        else if (!braked)
        {
            backLightObjectLeft.renderer.material = idleLightMaterial;
            backLightObjectRight.renderer.material = idleLightMaterial;
        }
    }

    void HandBrake()
    {
        GamePadState state = GamePad.GetState((PlayerIndex)0);

        if ((state.IsConnected && state.Buttons.X == 0) || Input.GetButton("Drift"))
            braked = true;
        else
            braked = false;
        if (braked)
        {
            FrontLeftWheel.brakeTorque = maxBrakeTorque;
            FrontRightWheel.brakeTorque = maxBrakeTorque;
            FrontLeftWheel.motorTorque = 0;
            FrontRightWheel.motorTorque = 0;
            if (rigidbody.velocity.magnitude > 1)
                SetSlip(slipForwardFriction, slipSidewayFriction);
            else
                SetSlip(1, 1);
            if (currentSpeed < 1 && currentSpeed > -1)
            {
                backLightObjectLeft.renderer.material = idleLightMaterial;
                backLightObjectRight.renderer.material = idleLightMaterial;
            }
            else
            {
                backLightObjectLeft.renderer.material = brakeLightMaterial;
                backLightObjectRight.renderer.material = brakeLightMaterial;
            }
        }
        else
        {
            FrontLeftWheel.brakeTorque = 0;
            FrontRightWheel.brakeTorque = 0;
            SetSlip(myForwardFriction, mySidewayFriction);
        }

    }


    void SetSlip(float currentForwardFriction, float currentSidewayFriction)
    {
        WheelFrictionCurve temporyWheel;
        temporyWheel = new WheelFrictionCurve();
        temporyWheel.stiffness = currentForwardFriction;
        temporyWheel.asymptoteSlip = BackRightWheel.forwardFriction.asymptoteSlip;
        temporyWheel.asymptoteValue = BackRightWheel.forwardFriction.asymptoteValue;
        temporyWheel.extremumSlip = BackRightWheel.forwardFriction.asymptoteSlip;
        temporyWheel.extremumValue = BackRightWheel.forwardFriction.asymptoteValue;
        BackRightWheel.forwardFriction = temporyWheel;

        temporyWheel.stiffness = currentForwardFriction;
        BackLeftWheel.forwardFriction = temporyWheel;


        temporyWheel.stiffness = currentSidewayFriction;
        BackRightWheel.sidewaysFriction = temporyWheel;

        temporyWheel.stiffness = currentSidewayFriction;
        BackLeftWheel.sidewaysFriction = temporyWheel;
    }
}
