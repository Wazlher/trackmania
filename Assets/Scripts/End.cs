﻿using UnityEngine;
using System.Collections;

public class End : MonoBehaviour
{
	public int maxTurn;
	public GameObject getTurn;
	public int nbPlayer;
	
	public string textTurn;
	public string textTurn2;

//	private GameObject displayTurn;
	private int actualTurn;
	private int actualTurn2;
	private bool[] check;
	private bool[] check2;
	private GameObject[] checkpoints;
	private GameObject lastCheckpoint;
	private GameObject lastCheckpoint2;
	private CameraSwap tmp;

	// Use this for initialization
	void Start ()
	{
		Vector3 vec = new Vector3(0.0f, 0.1f, 0.0f);

//		getTurn.guiText.text = "Turn " + actualTurn + "/2";
//		displayTurn = Instantiate(getTurn, vec, Quaternion.identity) as GameObject;

		actualTurn = 0;
		actualTurn2 = 0;
		lastCheckpoint = null;
		checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint");
		check = new bool[checkpoints.Length];
		int i = 0;
		while (i < check.Length)
		{
			check[i] = false;
			i++;
		}
		check2 = new bool[checkpoints.Length];
		int k = 0;
		while (k < check2.Length)
		{
			check2[k] = false;
			k++;
		}
	}

	public bool EndRun()
	{
		if (actualTurn >= maxTurn)
			return true;
		return false;
	}

	public int GetActualTurn()
	{
		return actualTurn;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag != "Player")
			return;
		tmp = (CameraSwap)other.transform.parent.transform.parent.GetComponent<CameraSwap>() as CameraSwap;
		bool test = true;
		int i = 0;
		if (tmp.player == 1)
		{
			while (i < check.Length)
			{
				if (!check[i])
				{
					test = false;
					break;
				}
				i++;
			}
		}
		else
		{
			while (i < check2.Length)
			{
				if (!check2[i])
				{
					test = false;
					break;
				}
				i++;
			}
		}
		if (test)
		{
			if (tmp.player == 1)
			{
				actualTurn++;
				i = 0;
				while (i < check.Length)
				{
					check[i] = false;
					i++;
				}
			}
			else
			{
				actualTurn2++;
				i = 0;
				while (i < check2.Length)
				{
					check2[i] = false;
					i++;
				}
			}
		}
	}
	
	void OnGUI()
	{
		textTurn = string.Format("Turn {0} / 2", actualTurn);
		GUIStyle turnStyle = new GUIStyle(GUI.skin.label);
		turnStyle.fontSize = 25;
		GUI.Label(new Rect (20, 20, 200, 70), textTurn, turnStyle);
		if (nbPlayer == 2)
		{
			textTurn2 = string.Format("Turn {0} / 2", actualTurn2);
			GUI.Label(new Rect(20, (Screen.height / 2) + (Screen.height / 20), 200, 70), textTurn2, turnStyle);
		}
	}

	// Update is called once per frame
	void Update ()
	{
		int i = 0;

		while (i < checkpoints.Length)
		{
			checkpoint c = checkpoints[i].GetComponent<checkpoint>();
			if (c.getIsActive2())
			{
				if (lastCheckpoint2 != checkpoints[i])
				{
					lastCheckpoint2 = checkpoints[i];
					check2[i] = true;
				}
				break;
			}
			i++;
		}

		i = 0;

		while (i < checkpoints.Length)
		{
			checkpoint c = checkpoints[i].GetComponent<checkpoint>();
			if (c.getIsActive())
			{
				if (lastCheckpoint != checkpoints[i])
				{
					lastCheckpoint = checkpoints[i];
					check[i] = true;
				}
				break;
			}
			i++;
		}
	}
}
